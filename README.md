# Description of project
Interview task for creating market checkout using ddd approach.
# Business requirements

- Design and implement market checkout component with readable API that calculates the total price of a number of items.
- Checkout mechanism can scan items and return actual price (is stateful).
- Our goods are priced individually.
- Some items are multi-priced: buy N of them, and they’ll cost you Y cents.

| Item | Price | Unit | Special price |
| ---- | ----- | ---- | ------------- |
| A | 40 | 3 | for 70 |
| B | 10 | 2 | for 15 |
| C | 30 | 4 | for 60 |
| D | 25 | 2 | for 40 |

- Client receives receipt containing list of all products with corresponding prices after payment.
- Some items are cheaper when bought together - buy item X with item Y and save Z cents.

| Items combination | Price | Promotion description |
| ----------------- | ----------------- | --------------------------------------------- |
| A + B + C | 70 instead of 80 | Buy 3 products, get the cheapest for free |
| A + B + C + D | 94,5 instead of 105 | Buy four different products and get 10% cheaper |

# Technical requirements

- The output of this task should be a project with buildable production ready service, that
can be executed independently of the source code.
- Project should include all tests, including unit, integration and acceptance tests.
- The service should expose REST api, without any UI.
- You can use gradle, maven or any other building tool.
- It ought to be written in Java 8.
- If requested, please use Spring or any other framework, otherwise, the choice is yours.
- Please include a short description of how to build and execute your project in e.g.
README.md file. It it's up to you to make your own assumptions about the code and
assignment. Each assumption should be captured as part of README.md file, so that we can review them before reading a code. 
There are no good and bad answers here. You can put there any other information that you would like to share with us like important classes, design overview, the rationale for your choices etc.

# Assumptions

- Market checkout is thread safe because every customer has own market-checkout station. In one time only one person can have access to single station. This assumption eliminates needs to design multi-thread solution because only one share resource is a database.
- Payment is performed via specialized separated external service like payu and will be treated is system as driven adapter.
- Prices added to storage include appropriate VAT, no needs to keep price in code included and not included VAT.
- ver 1 of app has no feature for limited products( e.g age limited products like tabacco or alcohol)

# Architectural and design notes
- Port and adapters chosen as well fit for DDD project

# Implementation notes