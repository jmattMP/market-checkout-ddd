package com.jmatt.market.checkout.application.commons;

import com.jmatt.market.checkout.domain.commons.specification.UnsatisfiedSpecification;

public class ExceptionMapper {

    private ExceptionMapper() {
        throw new IllegalStateException("Utility class");
    }

    public static String mapToMessageInException(UnsatisfiedSpecification unsatisfiedSpecification) {
        return String.format("Unsatisfied Specification %s of type %s and details %s",
                unsatisfiedSpecification.getSpecificationName(),
                unsatisfiedSpecification.getSpecificationType(),
                unsatisfiedSpecification.getSpecificationDetails());
    }
}
