package com.jmatt.market.checkout.application.dto;

import com.jmatt.market.checkout.application.dto.payment.PaymentMethod;
import com.jmatt.market.checkout.application.dto.payment.PaymentStatus;

import java.time.LocalDateTime;

public class PaymentSummary {
    private final PaymentStatus paymentStatus;
    private final LocalDateTime paymentProcessingDate;
    private final String orderId;
    private final PaymentMethod paymentMethod;

    public PaymentSummary(PaymentStatus paymentStatus, String orderId, PaymentMethod paymentMethod) {
        this.paymentStatus = paymentStatus;
        this.orderId = orderId;
        this.paymentMethod = paymentMethod;
        this.paymentProcessingDate = LocalDateTime.now();
    }

    public PaymentStatus getPaymentStatus() {
        return paymentStatus;
    }

    public LocalDateTime getPaymentProcessingDate() {
        return paymentProcessingDate;
    }

    public String getOrderId() {
        return orderId;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }
}
