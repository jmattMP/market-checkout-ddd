package com.jmatt.market.checkout.application.dto.order;

import com.jmatt.market.checkout.domain.commons.Money;

public class OrderItemDetails {
    private final String orderItemName;
    private final Money orderItemPrice;

    public OrderItemDetails(String orderItemName, Money orderItemPrice) {
        this.orderItemName = orderItemName;
        this.orderItemPrice = orderItemPrice;
    }

    public String getOrderItemName() {
        return orderItemName;
    }

    public Money getOrderItemPrice() {
        return orderItemPrice;
    }
}
