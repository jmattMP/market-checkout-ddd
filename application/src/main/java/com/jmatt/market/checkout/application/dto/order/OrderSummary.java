package com.jmatt.market.checkout.application.dto.order;

import com.jmatt.market.checkout.domain.commons.Money;
import com.jmatt.market.checkout.domain.order.OrderItem;
import com.jmatt.market.checkout.domain.rebate.Rebate;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

public class OrderSummary {
    private final Money totalPrice;
    private final Rebate rebate;
    private final Money totalPriceWithCalculatedRebate;
    private final Collection<OrderItem> orderItems;
    private final LocalDateTime orderDate;

    public OrderSummary(Money totalPrice, Rebate rebate, Money totalPriceWithCalculatedRebate, Collection<OrderItem> orderItems) {
        this.totalPrice = totalPrice;
        this.rebate = rebate;
        this.totalPriceWithCalculatedRebate = totalPriceWithCalculatedRebate;
        this.orderItems = orderItems;
        this.orderDate = LocalDateTime.now();
    }

    public Money getTotalPrice() {
        return totalPrice;
    }

    public Rebate getRebate() {
        return rebate;
    }

    public Money getTotalPriceWithCalculatedRebate() {
        return totalPriceWithCalculatedRebate;
    }

    public Map<UUID, OrderItemDetails> getOrderItems() {
        return orderItems.stream().collect(
                Collectors.toMap(
                        OrderItem::getProductId,
                        orderItem ->
                                new OrderItemDetails(orderItem.getItemName().getProductName(), orderItem.getPrice())));
    }

    public LocalDateTime getOrderDate() {
        return orderDate;
    }
}
