package com.jmatt.market.checkout.application.dto.payment;

import java.time.LocalDateTime;

public class PaymentDetails {
    private final String orderId;
    private final PaymentMethod paymentMethod;
    private final LocalDateTime localDateTime;

    public PaymentDetails(String orderId, PaymentMethod paymentMethod) {
        this.orderId = orderId;
        this.paymentMethod = paymentMethod;
        this.localDateTime = LocalDateTime.now();
    }

    public String getOrderId() {
        return orderId;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public LocalDateTime getLocalDateTime() {
        return localDateTime;
    }
}
