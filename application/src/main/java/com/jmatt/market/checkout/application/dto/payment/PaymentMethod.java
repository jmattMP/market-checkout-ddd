package com.jmatt.market.checkout.application.dto.payment;

public enum PaymentMethod {
    CARD, CASH;
}
