package com.jmatt.market.checkout.application.dto.payment;

public enum PaymentStatus {
    PAID, CANCELLED, FAILED;
}
