package com.jmatt.market.checkout.application.dto.receipt;

import com.jmatt.market.checkout.application.dto.order.OrderSummary;

import java.time.LocalDateTime;

public class ReceiptDetails {
    private final LocalDateTime issueDate;
    private final OrderSummary orderSummary;

    public ReceiptDetails(OrderSummary orderSummary) {
        this.orderSummary = orderSummary;
        this.issueDate = LocalDateTime.now();
    }

    public LocalDateTime getIssueDate() {
        return issueDate;
    }

    public OrderSummary getOrderSummary() {
        return orderSummary;
    }
}
