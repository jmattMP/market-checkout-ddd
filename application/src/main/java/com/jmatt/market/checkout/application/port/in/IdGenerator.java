package com.jmatt.market.checkout.application.port.in;

public interface IdGenerator {
    String generate();
}
