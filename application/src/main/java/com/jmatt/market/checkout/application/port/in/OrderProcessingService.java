package com.jmatt.market.checkout.application.port.in;

import com.jmatt.market.checkout.application.dto.order.OrderSummary;
import com.jmatt.market.checkout.domain.order.Order;
import com.jmatt.market.checkout.domain.order.OrderItem;
import com.jmatt.market.checkout.domain.product.Product;

import java.util.UUID;

/**
 * API for processing order
 */
public interface OrderProcessingService {

    /**
     * create new order
     *
     * @return created order
     */
    Order createOrder();

    /**
     * @param orderId identifier for order
     * @return Order based on orderId
     */
    Order getOrder(String orderId);

    /**
     * Adding product to order based on orderId
     *
     * @param product item added to oder
     * @param orderId identifier for order
     * @return OrderItem is a representation of product added to order
     */
    OrderItem addProduct(Product product, String orderId);

    /**
     * Removing product from order based on productId and orderId
     *
     * @param productId identifier for product
     * @param orderId   identifier for order
     * @return Order based on orderId
     */
    Order removeProduct(UUID productId, String orderId);

    /**
     * Confirm order based on orderId
     *
     * @param orderId identifier for order
     * @return OrderSummary contains summary of order
     */
    OrderSummary confirmOrder(String orderId);

    /**
     * Get summary of order based on orderId
     *
     * @param orderId identifier for order
     * @return OrderSummary contains summary of order
     */
    OrderSummary getOrderSummary(String orderId);

    /**
     * Cancel order based on orderId
     *
     * @param orderId identifier for order
     * @return true if order cancelled, false otherwise
     */
    boolean cancelOrder(String orderId);
}
