package com.jmatt.market.checkout.application.port.in;

public interface PasswordEncoder {
    String encode(String str);
}
