package com.jmatt.market.checkout.application.port.in;

import com.jmatt.market.checkout.application.dto.PaymentSummary;
import com.jmatt.market.checkout.application.dto.payment.PaymentDetails;

/**
 * API for paying for order
 */
public interface PayForOrderUseCase {
    /**
     * Payment processing based on paymentDetails
     *
     * @param paymentDetails contains information needed for payment
     * @return PaymentSummary response if payment succeed
     */
    PaymentSummary pay(PaymentDetails paymentDetails);
}
