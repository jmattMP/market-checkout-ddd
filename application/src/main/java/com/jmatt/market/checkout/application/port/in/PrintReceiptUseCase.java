package com.jmatt.market.checkout.application.port.in;

import com.jmatt.market.checkout.application.dto.receipt.ReceiptDetails;

/**
 * API for printing receipt
 */
public interface PrintReceiptUseCase {
    /**
     * print Receipt based on orderId
     *
     * @param orderId identifier for order
     * @return ReceiptDetails
     */
    ReceiptDetails getReceipt(String orderId);
}
