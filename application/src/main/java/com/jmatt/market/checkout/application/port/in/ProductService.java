package com.jmatt.market.checkout.application.port.in;

import com.jmatt.market.checkout.domain.product.Product;
import com.jmatt.market.checkout.domain.product.ProductCode;
import com.jmatt.market.checkout.domain.product.ProductTag;
import com.jmatt.market.checkout.domain.product.ProductType;

import java.util.Collection;
import java.util.UUID;

/**
 * API for searching products
 */
public interface ProductService {

    /**
     * Get product based on productId
     *
     * @param productId identifiers product
     * @return Product
     */
    Product getProductById(UUID productId);

    /**
     * Get products based on productCode
     *
     * @param productCode
     * @return Collection of products with corresponding product code
     */
    Collection<Product> getProductsByCode(ProductCode productCode);

    /**
     * @return all available products
     */
    Collection<Product> getAvailableProducts();

    /**
     * Get all available products based on product type or/and product tags
     *
     * @param productType describes Product type
     * @param productTags
     * @return all available products
     */
    Collection<Product> getAvailableProducts(ProductType productType, Collection<ProductTag> productTags);
}
