package com.jmatt.market.checkout.application.port.out;

import com.jmatt.market.checkout.domain.order.Order;

import java.util.Optional;

public interface OrderRepository {
    Optional<Order> getOrder(String orderId);

    boolean removeOrder(String orderId);

    boolean saveOrder(Order order);
}