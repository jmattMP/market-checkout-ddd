package com.jmatt.market.checkout.application.port.out;

import com.jmatt.market.checkout.application.dto.PaymentSummary;
import com.jmatt.market.checkout.application.dto.payment.PaymentDetails;

public interface PaymentGateway {
    /**
     * Redirect to payment gateway
     *
     * @param paymentDetails
     * @return
     */
    PaymentSummary getPaymentGateway(PaymentDetails paymentDetails);
}
