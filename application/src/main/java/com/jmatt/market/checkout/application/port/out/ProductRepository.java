package com.jmatt.market.checkout.application.port.out;

import com.jmatt.market.checkout.domain.product.Product;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;


public interface ProductRepository {
    Product create(Product product);

    Collection<Product> getAvailableProducts();

    Optional<Product> getProductById(UUID productId);
}
