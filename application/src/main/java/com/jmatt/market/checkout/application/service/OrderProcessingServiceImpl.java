package com.jmatt.market.checkout.application.service;

import com.jmatt.market.checkout.application.commons.ExceptionMapper;
import com.jmatt.market.checkout.application.dto.order.OrderSummary;
import com.jmatt.market.checkout.application.port.out.OrderRepository;
import com.jmatt.market.checkout.application.port.out.ProductRepository;
import com.jmatt.market.checkout.domain.commons.Money;
import com.jmatt.market.checkout.domain.commons.Validator;
import com.jmatt.market.checkout.domain.order.Order;
import com.jmatt.market.checkout.domain.order.OrderItem;
import com.jmatt.market.checkout.domain.order.OrderNotFoundException;
import com.jmatt.market.checkout.domain.product.Product;
import com.jmatt.market.checkout.domain.rebate.Rebate;
import com.jmatt.market.checkout.domain.rebate.RebateProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

public class OrderProcessingServiceImpl implements com.jmatt.market.checkout.application.port.in.OrderProcessingService {

    private static final Logger logger = LoggerFactory.getLogger(OrderProcessingServiceImpl.class);

    private final OrderRepository orderRepository;
    private final ProductRepository productRepository;
    private final Validator<Product> productValidator;
    private final RebateProvider rebateProvider;

    public OrderProcessingServiceImpl(OrderRepository orderRepository,
                                      ProductRepository productRepository,
                                      Validator<Product> productValidator,
                                      RebateProvider rebateProvider) {
        this.orderRepository = orderRepository;
        this.productRepository = productRepository;
        this.productValidator = productValidator;
        this.rebateProvider = rebateProvider;
    }

    @Override
    public OrderItem addProduct(Product product, String orderId) {
        logger.info("Test Log");
        if (!productValidator.isValid(product)) {
            String unsatisfiedSpecificationMessage = productValidator
                    .getUnsatisfiedSpecifications(product).stream()
                    .map(ExceptionMapper::mapToMessageInException)
                    .collect(Collectors.joining("\n"));
            throw new IllegalArgumentException(unsatisfiedSpecificationMessage);
        }
        Optional<Order> optionalOrder = orderRepository.getOrder(orderId);


        Order processedOrder = optionalOrder.orElseThrow(() -> {
            throw new OrderNotFoundException();
        });

        //todo lepsza walidacja
        var optionalItem = processedOrder.getOrderItems().stream().filter(orderItem -> orderItem.getProductId().equals(product.getProductId())).findAny();

        if (optionalItem.isPresent()) {
            throw new IllegalArgumentException("Product cannot be duplicated");
        }

        var addedItem = processedOrder.addItem(product);
        orderRepository.saveOrder(processedOrder);
        return addedItem;
    }

    @Override
    public Order createOrder() {
        return Order.createOrder();
    }

    @Override
    public Order getOrder(String orderId) {
        return orderRepository
                .getOrder(orderId)
                .orElseThrow(() -> {
                    throw new OrderNotFoundException(String.format("Order with id %s not exist", orderId));
                });
    }

    @Override
    public Order removeProduct(UUID productId, String orderId) {
        Optional<Order> optionalOrder = orderRepository.getOrder(orderId);
        Order processedOrder = optionalOrder.orElseThrow(() -> {
            throw new OrderNotFoundException();
        });
        return processedOrder.removeItem(productId);
    }

    @Override
    public OrderSummary confirmOrder(String orderId) {
        var order = orderRepository.getOrder(orderId).orElseThrow(OrderNotFoundException::new);
        order.submitOrder();
        orderRepository.saveOrder(order);
        return getOrderSummary(orderId);
        //todo czy wyrzucic produkty z repo tutaj czy przy placeniu?
    }

    @Override
    public OrderSummary getOrderSummary(String orderId) {
        Optional<Order> optionalOrder = orderRepository.getOrder(orderId);
        Order processedOrder = optionalOrder.orElseThrow(() -> {
            throw new OrderNotFoundException();
        });

        Rebate calculatedRebate = rebateProvider.calculateRebate(processedOrder);
        Money totalPriceWithDiscount = processedOrder.getTotalPrice().subtract(calculatedRebate.getDiscount());
        return new OrderSummary(processedOrder.getTotalPrice(), calculatedRebate, totalPriceWithDiscount, processedOrder.getOrderItems());
    }

    @Override
    public boolean cancelOrder(String orderId) {
        return orderRepository.removeOrder(orderId);
    }


    //TODO Add logs,

}
