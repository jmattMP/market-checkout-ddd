package com.jmatt.market.checkout.application.service;

import com.jmatt.market.checkout.application.dto.PaymentSummary;
import com.jmatt.market.checkout.application.dto.payment.PaymentDetails;
import com.jmatt.market.checkout.application.dto.payment.PaymentStatus;
import com.jmatt.market.checkout.application.port.in.PayForOrderUseCase;
import com.jmatt.market.checkout.application.port.out.OrderRepository;
import com.jmatt.market.checkout.application.port.out.PaymentGateway;
import com.jmatt.market.checkout.domain.order.OrderNotFoundException;

import java.util.Objects;

public class PayForOrder implements PayForOrderUseCase {

    private final PaymentGateway paymentGateway;
    private final OrderRepository orderRepository;

    public PayForOrder(PaymentGateway paymentGateway, OrderRepository orderRepository) {
        this.paymentGateway = Objects.requireNonNull(paymentGateway);
        this.orderRepository = Objects.requireNonNull(orderRepository);
    }

    @Override
    public PaymentSummary pay(PaymentDetails paymentDetails) {
        var paymentSummary = paymentGateway.getPaymentGateway(paymentDetails);
        if (paymentSummary.getPaymentStatus().equals(PaymentStatus.PAID)) {
            var optionalOrder = orderRepository.getOrder(paymentSummary.getOrderId());
            var order = optionalOrder.orElseThrow(OrderNotFoundException::new);
            order.confirmPayment();
            orderRepository.saveOrder(order);
            //TODO FIRE EVENT TO REMOVE PRODUCTS FROM PRODUCT REPOSITORY ?
        }
        return paymentSummary;
    }
}
