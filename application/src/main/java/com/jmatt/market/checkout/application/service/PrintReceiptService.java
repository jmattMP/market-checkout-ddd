package com.jmatt.market.checkout.application.service;

import com.jmatt.market.checkout.application.dto.receipt.ReceiptDetails;
import com.jmatt.market.checkout.application.port.in.OrderProcessingService;
import com.jmatt.market.checkout.application.port.in.PrintReceiptUseCase;
import com.jmatt.market.checkout.application.port.out.OrderRepository;
import com.jmatt.market.checkout.domain.order.OrderNotFoundException;
import com.jmatt.market.checkout.domain.order.OrderNotPaidException;
import com.jmatt.market.checkout.domain.order.OrderStatus;

public class PrintReceiptService implements PrintReceiptUseCase {

    private OrderRepository orderRepository;
    private OrderProcessingService orderProcessingService;

    public PrintReceiptService(OrderRepository orderRepository, OrderProcessingService orderProcessingService) {
        this.orderRepository = orderRepository;
        this.orderProcessingService = orderProcessingService;
    }

    @Override
    public ReceiptDetails getReceipt(String orderId) {
        var optionalOrder = orderRepository.getOrder(orderId);
        if (optionalOrder.isEmpty()) {
            throw new OrderNotFoundException(String.format("Order with id %s not found", orderId));
        }
        var order = optionalOrder.get();
        if (order.getProcessingStatus() != OrderStatus.PAID) {
            throw new OrderNotPaidException(String.format("Order with id %s not paid", orderId));
        }

        return new ReceiptDetails(orderProcessingService.getOrderSummary(orderId));
    }
}
