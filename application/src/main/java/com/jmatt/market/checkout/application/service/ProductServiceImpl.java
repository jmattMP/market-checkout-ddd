package com.jmatt.market.checkout.application.service;

import com.jmatt.market.checkout.application.port.in.ProductService;
import com.jmatt.market.checkout.domain.product.Product;
import com.jmatt.market.checkout.domain.product.ProductCode;
import com.jmatt.market.checkout.domain.product.ProductTag;
import com.jmatt.market.checkout.domain.product.ProductType;

import java.util.Collection;
import java.util.UUID;

public class ProductServiceImpl implements ProductService {

    @Override
    public Product getProductById(UUID productId) {
        return null;
    }

    @Override
    public Collection<Product> getProductsByCode(ProductCode productCode) {
        return null;
    }

    @Override
    public Collection<Product> getAvailableProducts() {
        return null;
    }

    @Override
    public Collection<Product> getAvailableProducts(ProductType productType, Collection<ProductTag> productTags) {
        return null;
    }
}
