package com.jmatt.market.checkout.application.validator;

import com.jmatt.market.checkout.application.port.out.ProductRepository;
import com.jmatt.market.checkout.domain.commons.specification.CompositeSpecification;
import com.jmatt.market.checkout.domain.commons.specification.SpecificationDetails;
import com.jmatt.market.checkout.domain.product.Product;

public class UniqueProductIdSpecification extends CompositeSpecification<Product> {

    private final ProductRepository productRepository;

    public UniqueProductIdSpecification(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public boolean isSatisfiedBy(Product candidate) {
        return productRepository.getProductById(candidate.getProductId()).isEmpty();
    }

    @Override
    public SpecificationDetails getSpecificationDetails() {
        return new SpecificationDetails("UniqueProductId", "Product Specification", "Product id is unique");
    }
}
