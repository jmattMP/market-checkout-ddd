package com.jmatt.market.checkout.application.service;

import com.jmatt.market.checkout.application.dto.PaymentSummary;
import com.jmatt.market.checkout.application.dto.order.OrderSummary;
import com.jmatt.market.checkout.application.dto.payment.PaymentDetails;
import com.jmatt.market.checkout.application.dto.payment.PaymentMethod;
import com.jmatt.market.checkout.application.dto.receipt.ReceiptDetails;
import com.jmatt.market.checkout.application.port.in.OrderProcessingService;
import com.jmatt.market.checkout.application.port.in.PayForOrderUseCase;
import com.jmatt.market.checkout.application.port.in.PrintReceiptUseCase;
import com.jmatt.market.checkout.application.port.out.OrderRepository;
import com.jmatt.market.checkout.application.port.out.PaymentGateway;
import com.jmatt.market.checkout.application.port.out.ProductRepository;
import com.jmatt.market.checkout.domain.commons.Money;
import com.jmatt.market.checkout.domain.commons.Validator;
import com.jmatt.market.checkout.domain.order.Order;
import com.jmatt.market.checkout.domain.order.OrderItem;
import com.jmatt.market.checkout.domain.product.*;
import com.jmatt.market.checkout.domain.rebate.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.Month;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


class OrderProcessingServiceImplTest {

    private OrderProcessingService orderProcessingService;

    private OrderRepository orderRepository;
    private ProductRepository productRepository;
    private Validator<Product> productValidator;
    private RebateProvider rebateProvider;

    private PayForOrderUseCase payForOrderUseCase;
    private PrintReceiptUseCase printReceiptUseCase;
    private String processedOrderId;
    private PaymentSummary processedPaymentSummary;
    private ReceiptDetails receiptDetails;
    private OrderItem processedOrderItem;
    private PaymentGateway paymentGateway;
    private Order processedOrder;
    private OrderSummary orderSummary;
    private PaymentDetails paymentDetails;
    private Product discountedProductA;
    private Product discountedSecondProductA;
    private Product discountedThirdProductA;
    private Product discountedProductB;
    private Product discountedProductC;
    private Product discountedProductD;

    @BeforeEach
    void setUp() {
        orderRepository = mock(OrderRepository.class);
        paymentGateway = mock(PaymentGateway.class);
        ProductRepository productRepository = mock(ProductRepository.class);
        Validator<Product> productValidator = mock(Validator.class);
        when(productValidator.isValid(any())).thenReturn(true);


        ProductName productNameA = ProductName.from("Product A");
        ProductType productTypeA = ProductType.GROCERIES;
        Collection<ProductTag> productTagsA = List.of(ProductTag.from("healthy food"));
        ProductCode productCodeA = ProductCode.ProductSkuGenerator.generateSkuBasedOn(productNameA, productTypeA, productTagsA);

        ProductName productNameB = ProductName.from("Product B");
        ProductType productTypeB = ProductType.GROCERIES;
        Collection<ProductTag> productTagsB = List.of(ProductTag.from("meat"));
        ProductCode productCodeB = ProductCode.ProductSkuGenerator.generateSkuBasedOn(productNameB, productTypeB, productTagsB);

        ProductName productNameC = ProductName.from("Product C");
        ProductType productTypeC = ProductType.COSMETICS;
        Collection<ProductTag> productTagsC = List.of(ProductTag.from("Deodorant"), ProductTag.from("Fresh"));
        ProductCode productCodeC = ProductCode.ProductSkuGenerator.generateSkuBasedOn(productNameC, productTypeC, productTagsC);

        ProductName productNameD = ProductName.from("Product D");
        ProductType productTypeD = ProductType.HEALTH;
        Collection<ProductTag> productTagsD = List.of(ProductTag.from("Vitamins"), ProductTag.from("Minerals"));
        ProductCode productCodeD = ProductCode.ProductSkuGenerator.generateSkuBasedOn(productNameD, productTypeD, productTagsD);

        discountedProductA = new Product(
                productNameA, productCodeA, Money.from(40), LocalDate.of(2021, Month.MAY, 1), productTypeA, productTagsA);
        discountedSecondProductA = new Product(
                productNameA, productCodeA, Money.from(40), LocalDate.of(2021, Month.MAY, 1), productTypeA, productTagsA);
        discountedThirdProductA = new Product(
                productNameA, productCodeA, Money.from(40), LocalDate.of(2021, Month.MAY, 1), productTypeA, productTagsA);
        discountedProductB = new Product(
                ProductName.from("Product B"), productCodeB, Money.from(10), LocalDate.of(2020, Month.MAY, 1), productTypeB, productTagsB);
        discountedProductC = new Product(
                ProductName.from("Product C"), productCodeC, Money.from(30), LocalDate.of(2021, Month.MAY, 1), productTypeC, productTagsC);
        discountedProductD = new Product(
                ProductName.from("Product D"), productCodeD, Money.from(25), LocalDate.of(2021, Month.MAY, 1), productTypeD, productTagsD);

        Collection<RebatePolicy> rebatePolicies = List.of(
                new MoreSameProductsWithDiscountRebate(discountedProductA.getProductCode(), 3, Money.from(50)),
                new TheCheapestForFree(3),
                new TogetherCheaperRebate(List.of(discountedProductA, discountedProductB, discountedProductC, discountedProductD), 10.0));
        RebateProvider rebateProvider = new DefaultRebateProvider(rebatePolicies);
        orderProcessingService = new OrderProcessingServiceImpl(orderRepository, productRepository, productValidator, rebateProvider);
        payForOrderUseCase = new PayForOrder(paymentGateway, orderRepository);

        paymentDetails = new PaymentDetails(processedOrderId, PaymentMethod.CARD);

        printReceiptUseCase = new PrintReceiptService(orderRepository, orderProcessingService);
    }

    @Test
    void processOrder() {
        var newOrder = orderProcessingService.createOrder();

        when(orderRepository.getOrder(anyString())).thenReturn(Optional.of(newOrder));

        orderProcessingService.addProduct(discountedProductA, newOrder.getOrderId());
        orderProcessingService.addProduct(discountedSecondProductA, newOrder.getOrderId());
        orderProcessingService.addProduct(discountedThirdProductA, newOrder.getOrderId());
        orderProcessingService.addProduct(discountedProductB, newOrder.getOrderId());
        orderProcessingService.addProduct(discountedProductC, newOrder.getOrderId());
        orderProcessingService.addProduct(discountedProductD, newOrder.getOrderId());

        orderProcessingService.removeProduct(discountedProductC.getProductId(), newOrder.getOrderId());

        var orderSummary = orderProcessingService.confirmOrder(newOrder.getOrderId());

        //todo check it from business perspective -> especially rebates
        assertThat(orderSummary.getOrderItems()).hasSize(5);
        //   assertThat(orderSummary.getOrderItems()).doesNotContainKeys(discountedProductC.getProductId());
        assertThat(orderSummary.getOrderItems()).containsKeys(discountedProductB.getProductId());
        assertThat(orderSummary.getRebate().getDiscount()).isEqualByComparingTo(Money.from(10.50));
        assertThat(orderSummary.getTotalPrice()).isEqualByComparingTo(Money.from(145));
        assertThat(orderSummary.getTotalPriceWithCalculatedRebate()).isEqualByComparingTo(Money.from(134.5));
        //  assertThat(orderSummary.getOrderDate()).isEqualTo(newOrder.getCreationDate());
    }
}