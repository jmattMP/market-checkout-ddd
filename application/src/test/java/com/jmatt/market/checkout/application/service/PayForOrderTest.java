package com.jmatt.market.checkout.application.service;

import com.jmatt.market.checkout.application.dto.PaymentSummary;
import com.jmatt.market.checkout.application.dto.payment.PaymentDetails;
import com.jmatt.market.checkout.application.dto.payment.PaymentMethod;
import com.jmatt.market.checkout.application.dto.payment.PaymentStatus;
import com.jmatt.market.checkout.application.port.in.PayForOrderUseCase;
import com.jmatt.market.checkout.application.port.out.OrderRepository;
import com.jmatt.market.checkout.application.port.out.PaymentGateway;
import com.jmatt.market.checkout.domain.order.Order;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
class PayForOrderTest {

    private PayForOrderUseCase payForOrderUseCase;

    @Mock
    private PaymentGateway paymentGateway;
    @Mock
    private OrderRepository orderRepository;

    @BeforeEach
    void setUp() {
        payForOrderUseCase = new PayForOrder(paymentGateway, orderRepository);
    }

    @Test
    void payForOrder() {
        var order = Order.createOrder();
        PaymentDetails paymentDetails = new PaymentDetails(order.getOrderId(), PaymentMethod.CARD);
        given(paymentGateway.getPaymentGateway(paymentDetails)).willReturn(new PaymentSummary(PaymentStatus.PAID, order.getOrderId(), PaymentMethod.CARD));
        given(orderRepository.getOrder(eq(order.getOrderId()))).willReturn(Optional.of(order));
        PaymentSummary paymentSummary = payForOrderUseCase.pay(paymentDetails);

        assertThat(paymentSummary.getPaymentMethod()).isEqualByComparingTo(PaymentMethod.CARD);
        assertThat(paymentSummary.getPaymentStatus()).isEqualByComparingTo(PaymentStatus.PAID);
    }
}