package com.jmatt.market.checkout.application.service;

import com.jmatt.market.checkout.application.dto.order.OrderSummary;
import com.jmatt.market.checkout.application.port.in.OrderProcessingService;
import com.jmatt.market.checkout.application.port.in.PrintReceiptUseCase;
import com.jmatt.market.checkout.application.port.out.OrderRepository;
import com.jmatt.market.checkout.domain.commons.Money;
import com.jmatt.market.checkout.domain.order.Order;
import com.jmatt.market.checkout.domain.rebate.Rebate;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
class ReceiptPrintingServiceTest {

    private PrintReceiptUseCase printReceiptUseCase;

    @Mock
    private OrderRepository orderRepository;
    @Mock
    private OrderProcessingService orderProcessingService;

    @BeforeEach
    void setUp() {
        printReceiptUseCase = new PrintReceiptService(orderRepository, orderProcessingService);
    }

    @Test
    void printReceipt() {
        var order = Order.createOrder();
        order.confirmPayment();

        given(orderRepository.getOrder(order.getOrderId())).willReturn(Optional.of(order));
        given(orderProcessingService.getOrderSummary(order.getOrderId())).willReturn(
                new OrderSummary(Money.from(120.0), new Rebate(Money.from(20.0), "20 bucks less"), Money.from(100.0), Collections.emptyList()));

        var receiptDetails = printReceiptUseCase.getReceipt(order.getOrderId());

        assertThat(receiptDetails.getOrderSummary().getTotalPrice()).isEqualByComparingTo(Money.from(120.0));
        assertThat(receiptDetails.getOrderSummary().getRebate()).isEqualToComparingFieldByField(new Rebate(Money.from(20.0), "20 bucks less"));
        assertThat(receiptDetails.getOrderSummary().getTotalPriceWithCalculatedRebate()).isEqualByComparingTo(Money.from(100.0));
    }
}