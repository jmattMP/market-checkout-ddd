package com.jmatt.common.adapter;

import com.jmatt.market.checkout.application.port.in.IdGenerator;

import java.util.UUID;

public class UuidGenerator implements IdGenerator {

    @Override
    public String generate() {
        return UUID.randomUUID().toString();
    }
}
