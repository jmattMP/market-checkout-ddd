package com.jmatt.market.checkout.domain.commons;

import java.time.LocalDateTime;
import java.util.UUID;

public class IdentityGenerator {
    public static String generateIdentityBasedOnDate() {
        return LocalDateTime.now() + "-" + UUID.randomUUID().toString();
    }

    private IdentityGenerator() {
        throw new IllegalStateException("Utility class");
    }
}