package com.jmatt.market.checkout.domain.commons;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Objects;

public class Money implements Comparable<Money> {
    private final BigDecimal amount;
    private final Currency currency;

    public static final Currency DEFAULT_CURRENCY = Currency.getInstance("EUR");

    public static final Money ZERO = new Money(BigDecimal.ZERO, DEFAULT_CURRENCY);

    private Money(BigDecimal amount, Currency currency) {
        this.currency = currency;
        this.amount = Objects.requireNonNull(amount);
    }

    public static Money from(double amount) {
        return new Money(BigDecimal.valueOf(amount), DEFAULT_CURRENCY);
    }

    public static Money from(double amount, Currency currency) {
        return new Money(BigDecimal.valueOf(amount), currency);
    }

    public static Money from(BigDecimal bigDecimal) {
        return new Money(bigDecimal, DEFAULT_CURRENCY);
    }

    public static Money from(BigDecimal bigDecimal, Currency currency) {
        return new Money(bigDecimal, currency);
    }

    public Money add(Money other) {
        checkCurrency(other);
        return new Money(amount.add(other.amount), DEFAULT_CURRENCY);
    }

    private void checkCurrency(Money other) {
        if (!compatibleCurrency(other)) {
            throw new IllegalArgumentException("Currency mismatch");
        }
    }

    public Money subtract(Money other) {
        checkCurrency(other);
        return new Money(amount.subtract(other.amount), DEFAULT_CURRENCY);
    }

    public Money multiplyBy(double multiplier) {
        return Money.from(this.getAmount().multiply(BigDecimal.valueOf(multiplier)));
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Currency getCurrency() {
        return currency;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Money) {
            Money money = (Money) obj;
            return compatibleCurrency(money) &&
                    this.getAmount().equals(money.getAmount());
        }
        return false;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + amount.hashCode();
        return result;
    }

    public boolean greaterThan(Money other) {
        checkCurrency(other);
        return this.getAmount().compareTo(other.getAmount()) > 0;
    }

    public int compareTo(Money other) {
        return amount.compareTo(other.getAmount());
    }

    private boolean compatibleCurrency(Money money) {
        return this.getCurrency().equals(money.getCurrency());
    }
}

