package com.jmatt.market.checkout.domain.commons;

import org.apache.commons.text.WordUtils;

public class NameUtil {

    public static String capitalizeFully(String string) {
        final char[] delimiters = {' ', '_'};
        return WordUtils.capitalizeFully(string, delimiters);
    }


    private NameUtil() {
        throw new IllegalStateException("Utility class");
    }
}
