package com.jmatt.market.checkout.domain.commons;

import com.jmatt.market.checkout.domain.commons.specification.UnsatisfiedSpecification;

import java.util.Collection;

public interface Validator<T> {
    boolean isValid(T entity);

    Collection<UnsatisfiedSpecification> getUnsatisfiedSpecifications(T entity);
}
