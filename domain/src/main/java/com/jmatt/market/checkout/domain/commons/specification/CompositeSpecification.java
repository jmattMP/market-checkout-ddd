package com.jmatt.market.checkout.domain.commons.specification;

import java.util.Arrays;
import java.util.List;

public abstract class CompositeSpecification<T> implements Specification<T> {

    public Specification<T> and(Specification<T> other) {
        return new AndSpecification<>(this, other);
    }

    public Specification<T> or(Specification<T> other) {
        return new OrSpecification<>(this, other);
    }

    public Specification<T> not() {
        return new NotSpecification<>(this);
    }

    @SuppressWarnings("unchecked")
    public Specification<T> conjunction(Specification<T>... others) {
        List<Specification<T>> list = Arrays.asList(others);
        list.add(this);
        return new Conjunction<>(list);
    }
}

