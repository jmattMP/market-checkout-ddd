package com.jmatt.market.checkout.domain.commons.specification;

import java.util.Collection;

public class Conjunction<T> extends CompositeSpecification<T> {

    private Collection<Specification<T>> list;

    public Conjunction(Collection<Specification<T>> list) {
        this.list = list;
    }

    @Override
    public boolean isSatisfiedBy(T candidate) {
        for (Specification<T> spec : list) {
            if (!spec.isSatisfiedBy(candidate))
                return false;
        }

        return true;
    }

    @Override
    public SpecificationDetails getSpecificationDetails() {
        return new SpecificationDetails("ConjunctionSpecification", "ConjunctionType", "ConjunctionDetails");
    }

}
