package com.jmatt.market.checkout.domain.commons.specification;

public class NotSpecification<T> extends CompositeSpecification<T> {
    private Specification<T> wrapped;

    public NotSpecification(Specification<T> wrapped) {
        this.wrapped = wrapped;
    }

    public boolean isSatisfiedBy(T candidate) {
        return !wrapped.isSatisfiedBy(candidate);
    }

    @Override
    public SpecificationDetails getSpecificationDetails() {
        return new SpecificationDetails("NotSpecification", "NotSpecification", "NotSpecification");
    }
}
