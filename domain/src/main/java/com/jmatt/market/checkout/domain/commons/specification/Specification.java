package com.jmatt.market.checkout.domain.commons.specification;

public interface Specification<T> {
    boolean isSatisfiedBy(T candidate);

    Specification<T> and(Specification<T> other);

    Specification<T> or(Specification<T> other);

    @SuppressWarnings("unchecked")
    Specification<T> conjunction(Specification<T>... others);

    Specification<T> not();

    SpecificationDetails getSpecificationDetails();
}
