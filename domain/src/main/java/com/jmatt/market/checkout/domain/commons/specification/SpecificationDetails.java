package com.jmatt.market.checkout.domain.commons.specification;

public class SpecificationDetails {
    private final String name;
    private final String type;
    private final String details;

    public SpecificationDetails(String name, String type, String details) {
        this.name = name;
        this.type = type;
        this.details = details;
    }

    public String getName() {
        return name;
    }

    public String getDetails() {
        return details;
    }

    public String getType() {
        return type;
    }
}
