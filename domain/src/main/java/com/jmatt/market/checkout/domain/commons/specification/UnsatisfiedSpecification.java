package com.jmatt.market.checkout.domain.commons.specification;

public class UnsatisfiedSpecification {
    private final String specificationName;
    private final String specificationType;
    private final String specificationDetails;

    public UnsatisfiedSpecification(SpecificationDetails specificationDetails) {
        this.specificationName = specificationDetails.getName();
        this.specificationType = specificationDetails.getType();
        this.specificationDetails = specificationDetails.getDetails();
    }

    public String getSpecificationName() {
        return specificationName;
    }

    public String getSpecificationType() {
        return specificationType;
    }

    public String getSpecificationDetails() {
        return specificationDetails;
    }
}
