package com.jmatt.market.checkout.domain.order;

import com.jmatt.market.checkout.domain.commons.IdentityGenerator;
import com.jmatt.market.checkout.domain.commons.Money;
import com.jmatt.market.checkout.domain.product.Product;

import java.time.LocalDate;
import java.util.*;

public class Order {
    private final String orderId;
    private final LocalDate creationDate;
    private final Collection<OrderItem> orderItems;
    private Money totalPrice;
    private OrderStatus orderStatus;

    private Order(String orderId, LocalDate creationDate, Collection<OrderItem> orderItems, Money totalPrice, OrderStatus orderStatus) {
        this.orderId = orderId;
        this.creationDate = creationDate;
        this.orderItems = new ArrayList<>(orderItems);
        this.totalPrice = totalPrice;
        this.orderStatus = orderStatus;
    }

    public static Order createOrder() {
        return new Order(IdentityGenerator.generateIdentityBasedOnDate(), LocalDate.now(), Collections.emptyList(), Money.ZERO, OrderStatus.NEW);
    }

    public Order submitOrder() {
        totalPrice = orderItems.stream().map(OrderItem::getPrice).reduce(Money.ZERO, Money::add);
        return new Order(this.orderId, this.creationDate, this.orderItems, totalPrice, OrderStatus.SUBMITTED);
    }

    public OrderItem addItem(Product product) {
        OrderItem orderItem = new OrderItem(product);
        orderItems.add(orderItem);
        totalPrice = totalPrice.add(product.getProductPrice());
        return orderItem;
    }

    public Order removeItem(UUID productId) {
        Optional<OrderItem> optionalOrderItem = orderItems.stream().filter(orderItem -> orderItem.getProductId().equals(productId)).findAny();
        if (optionalOrderItem.isEmpty()) {
            throw new OrderItemNotFoundException("OrderItem not found in order");
        }
        boolean isRemoved = orderItems.remove(optionalOrderItem.get());
        if (isRemoved) {
            totalPrice = totalPrice.subtract(optionalOrderItem.get().getPrice());
        }
        return this;
    }

    public String getOrderId() {
        return orderId;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public Collection<OrderItem> getOrderItems() {
        return orderItems;
    }

    public Money getTotalPrice() {
        return totalPrice;
    }

    public OrderStatus getProcessingStatus() {
        return orderStatus;
    }

    public void confirmPayment() {
        orderStatus = OrderStatus.PAID;
    }

    public void archiveOrder() {
        orderStatus = OrderStatus.ARCHIVED;
    }
}
