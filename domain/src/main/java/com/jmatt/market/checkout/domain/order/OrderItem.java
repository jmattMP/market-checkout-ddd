package com.jmatt.market.checkout.domain.order;

import com.jmatt.market.checkout.domain.commons.Money;
import com.jmatt.market.checkout.domain.product.Product;
import com.jmatt.market.checkout.domain.product.ProductCode;
import com.jmatt.market.checkout.domain.product.ProductName;

import java.util.Objects;
import java.util.UUID;

/*
todo further improvements
Order has potential to be Aggregate root and OrderItem as element of aggregate Order with OrderStatus and all Order related VO-s
Please consider to redesign this class as member of aggregate with accessibility only from Order as Aggregate root
Please consider that orderItem contains information about order, as well order item contains quantity, price of single item, total price
 */
public class OrderItem {
    private UUID productId;
    private ProductCode productCode;
    private ProductName itemName;
    private Money price;

    public OrderItem(Product product) {
        Objects.requireNonNull(product);
        this.price = product.getProductPrice();
        this.productId = product.getProductId();
        this.itemName = product.getProductName();
        this.productCode = product.getProductCode();
    }

    public Money getPrice() {
        return price;
    }

    public ProductCode getProductCode() {
        return productCode;
    }

    public ProductName getItemName() {
        return itemName;
    }

    public UUID getProductId() {
        return productId;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof OrderItem) {
            OrderItem item = (OrderItem) obj;
            return item.getProductId().equals(((OrderItem) obj).getProductId());
        }
        return false;
    }
}
