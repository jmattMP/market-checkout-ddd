package com.jmatt.market.checkout.domain.order;

public class OrderItemNotFoundException extends RuntimeException {
    public OrderItemNotFoundException() {
        super();
    }

    public OrderItemNotFoundException(String message) {
        super(message);
    }
}
