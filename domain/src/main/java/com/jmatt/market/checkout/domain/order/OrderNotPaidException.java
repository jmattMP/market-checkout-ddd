package com.jmatt.market.checkout.domain.order;

public class OrderNotPaidException extends RuntimeException {
    public OrderNotPaidException() {
        super();
    }

    public OrderNotPaidException(String message) {
        super(message);
    }
}
