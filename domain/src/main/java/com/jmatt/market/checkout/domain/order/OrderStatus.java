package com.jmatt.market.checkout.domain.order;

public enum OrderStatus {
    NEW, SUBMITTED, PAID, ARCHIVED
}
