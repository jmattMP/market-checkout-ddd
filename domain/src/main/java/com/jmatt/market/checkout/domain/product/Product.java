package com.jmatt.market.checkout.domain.product;

import com.jmatt.market.checkout.domain.commons.Money;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;
import java.util.UUID;

public class Product {
    private final UUID productId;
    private ProductName productName;
    private ProductCode productCode;
    private Money productPrice;
    private LocalDate productExpirationDate;
    private ProductType productType;
    private Collection<ProductTag> productTags;

    public Product(ProductName productName, ProductCode productCode, Money productPrice, LocalDate productExpirationDate, ProductType productType, Collection<ProductTag> productTags) {
        this.productId = UUID.randomUUID();
        this.productName = productName;
        this.productCode = productCode;
        this.productPrice = productPrice;
        this.productExpirationDate = productExpirationDate;
        this.productType = productType;
        this.productTags = productTags;
    }

    public ProductCode getProductCode() {
        return productCode;
    }

    public UUID getProductId() {
        return productId;
    }

    public ProductName getProductName() {
        return productName;
    }

    public Money getProductPrice() {
        return productPrice;
    }

    public Optional<LocalDate> getProductExpirationDate() {
        return Optional.ofNullable(productExpirationDate);
    }

    public Optional<ProductType> getProductType() {
        return Optional.ofNullable(productType);
    }

    public Collection<ProductTag> getProductTags() {
        if (null == productTags) {
            return Collections.emptyList();
        }
        return productTags;
    }

    @Override
    public int hashCode() {
        return productCode.hashCode() + productPrice.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Product) {
            Product other = (Product) obj;
            return productCode.getProductCodeName().equals(other.getProductCode().getProductCodeName()) &&
                    productPrice.equals(productPrice);
        }
        return false;
    }

    //Builder Class
    public static class ProductBuilder {

        // required parameters
        private final ProductName productName;
        private final ProductCode productCode;
        private final Money productPrice;

        // optional parameters
        private LocalDate productExpirationDate;
        private ProductType productType;
        private Collection<ProductTag> productTags;

        public ProductBuilder(ProductName productName, ProductCode productCode, Money productPrice) {
            this.productName = productName;
            this.productCode = productCode;
            this.productPrice = productPrice;
        }

        public ProductBuilder setProductExpirationDate(LocalDate productExpirationDate) {
            this.productExpirationDate = productExpirationDate;
            return this;
        }

        public ProductBuilder setProductType(ProductType productType) {
            this.productType = productType;
            return this;
        }

        public ProductBuilder setProductTags(Collection<ProductTag> productTags) {
            this.productTags = productTags;
            return this;
        }

        public Product build() {
            return new Product(this.productName, this.productCode, this.productPrice, this.productExpirationDate, this.productType, this.productTags);
        }
    }
}
