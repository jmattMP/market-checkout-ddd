package com.jmatt.market.checkout.domain.product;

import java.util.Collection;
import java.util.Optional;


public class ProductCode {
    private final String productCodeName;

    private ProductCode(String productCodeName) {
        this.productCodeName = productCodeName;
    }

    public String getProductCodeName() {
        return productCodeName;
    }

    public static class ProductSkuGenerator {
        public static ProductCode generateSkuBasedOn(ProductName productName, ProductType productType, Collection<ProductTag> productTags) {
            if (null == productName || null == productType) {
                throw new IllegalArgumentException("Product name and product Type could not be null");
            }

            String productNameCode = productName.getProductName().substring(0, 2).toUpperCase();
            String productTypeCode = productType.name().substring(0, 2).toUpperCase();

            if (null == productTags || productTags.isEmpty()) {
                return new ProductCode(productNameCode + "-" + productTypeCode);
            }

            Optional<String> optionalProductTagCodes = productTags.stream().map(productTag -> productTag.getProductTag().substring(0, 2).toUpperCase()).reduce((s, s2) -> s + "-" + s2);

            var productTagCodes = optionalProductTagCodes.orElseThrow();
            return new ProductCode(productNameCode + "-" + productTypeCode + "-" + productTagCodes);
        }
    }
}
