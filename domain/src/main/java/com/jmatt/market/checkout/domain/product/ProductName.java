package com.jmatt.market.checkout.domain.product;

import com.jmatt.market.checkout.domain.commons.NameUtil;

public class ProductName {
    private final String productName;

    public static ProductName from(String productName) {
        if (null == productName || productName.isBlank() || productName.isEmpty() || productName.length() < 5) {
            throw new IllegalArgumentException("Product name cannot be null, blank, empty or less then 5 chars.");
        }
        String fullyCapitalizedProductName = NameUtil.capitalizeFully(productName);
        return new ProductName(fullyCapitalizedProductName);
    }

    private ProductName(String productName) {
        this.productName = productName;
    }

    public String getProductName() {
        return productName;
    }
}
