package com.jmatt.market.checkout.domain.product;

public class ProductNotFoundException extends RuntimeException {
    public ProductNotFoundException() {
        super();
    }
}
