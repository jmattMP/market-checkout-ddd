package com.jmatt.market.checkout.domain.product;

import com.jmatt.market.checkout.domain.commons.NameUtil;

public class ProductTag {
    private final String productTag;

    public static ProductTag from(String productTag) {
        if (null == productTag || productTag.isBlank() || productTag.isEmpty() || productTag.length() < 4) {
            throw new IllegalArgumentException("Product tag cannot be null, blank, empty or less then 4 chars.");
        }
        String fullyCapitalizedProductTag = NameUtil.capitalizeFully(productTag);
        return new ProductTag(fullyCapitalizedProductTag);
    }

    private ProductTag(String productTag) {
        this.productTag = productTag;
    }

    public String getProductTag() {
        return productTag;
    }
}
