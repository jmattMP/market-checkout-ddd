package com.jmatt.market.checkout.domain.product;

public enum ProductType {

    CLOTHING("Clothing", "CLO"),
    COSMETICS("Cosmetics", "COS"),
    DETERGENTS("Detergents", "DET"),
    ELECTRONICS("Electronics", "ELE"),
    ENTERTAINMENT("Entertainment", "ENT"),
    FOR_ANIMAL("For animal", "ANI"),
    FOR_KIDS("For kids", "KID"),
    GROCERIES("Groceries", "GRO"),
    HEALTH("Health", "HEA"),
    HOUSEHOLD_GOODS("Household goods", "HOU"),
    HOUSE_AND_GARDEN("House and garden", "GAR"),
    INDUSTRIAL_GOODS("Industrial goods", "IND"),
    RELAX("Relax", "REL"),
    SPORTS_EQUIPMENT("Sports equipment", "SPO");

    private final String printableName;
    private final String productTypeCode;

    ProductType(String printableName, String productTypeCode) {
        if (productTypeCode.length() != 3) {
            throw new IllegalArgumentException("Product type code must have 3 chars");
        }
        this.printableName = printableName;
        this.productTypeCode = productTypeCode;
    }

    public String getPrintableName() {
        return printableName;
    }

    public String getProductTypeCode() {
        return productTypeCode;
    }
}
