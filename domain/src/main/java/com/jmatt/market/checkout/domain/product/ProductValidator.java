package com.jmatt.market.checkout.domain.product;

import com.jmatt.market.checkout.domain.commons.Validator;
import com.jmatt.market.checkout.domain.commons.specification.Specification;
import com.jmatt.market.checkout.domain.commons.specification.UnsatisfiedSpecification;

import java.util.Collection;

import static java.util.stream.Collectors.toList;

public class ProductValidator implements Validator<Product> {

    private final Collection<Specification<Product>> rules;

    public ProductValidator(Collection<Specification<Product>> rules) {
        this.rules = rules;
    }

    @Override
    public boolean isValid(Product entity) {
        return getUnsatisfiedSpecifications(entity).isEmpty();
    }

    @Override
    public Collection<UnsatisfiedSpecification> getUnsatisfiedSpecifications(Product entity) {
        return rules.stream()
                .takeWhile(productSpecification -> !productSpecification.isSatisfiedBy(entity))
                .map(productSpecification -> new UnsatisfiedSpecification(productSpecification.getSpecificationDetails()))
                .collect(toList());
    }
}
