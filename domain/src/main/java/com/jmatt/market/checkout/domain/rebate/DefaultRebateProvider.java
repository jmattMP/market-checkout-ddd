package com.jmatt.market.checkout.domain.rebate;

import com.jmatt.market.checkout.domain.commons.Money;
import com.jmatt.market.checkout.domain.order.Order;
import com.jmatt.market.checkout.domain.order.OrderItem;
import com.jmatt.market.checkout.domain.product.Product;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.stream.Collectors;

public class DefaultRebateProvider implements RebateProvider {
    private final Collection<RebatePolicy> rebatePolicies;

    public DefaultRebateProvider(Collection<RebatePolicy> rebatePolicies) {
        this.rebatePolicies = new ArrayList<>(rebatePolicies);
    }

    @Override
    public Rebate calculateRebate(Order order) {
        if (order.getOrderItems().isEmpty()) {
            return new Rebate(Money.ZERO, "NO APPLIED");
        }

        Collection<Rebate> rebates = new ArrayList<>();

        Collection<OrderItem> orderItems = order.getOrderItems();

        Collection<Product> products = orderItems.stream()
                .map(oi -> new Product.ProductBuilder(oi.getItemName(), oi.getProductCode(), oi.getPrice()).build())
                .collect(Collectors.toList());

        rebatePolicies.forEach(rebatePolicy -> rebates.add(rebatePolicy.calculateRebate(products)));

        return rebates.stream().max(Comparator.comparing(Rebate::getDiscount)).get();
    }
}
