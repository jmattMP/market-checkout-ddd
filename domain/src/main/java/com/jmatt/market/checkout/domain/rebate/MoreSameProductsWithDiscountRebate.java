package com.jmatt.market.checkout.domain.rebate;

import com.jmatt.market.checkout.domain.commons.DomainPolicy;
import com.jmatt.market.checkout.domain.commons.Money;
import com.jmatt.market.checkout.domain.product.Product;
import com.jmatt.market.checkout.domain.product.ProductCode;

import java.util.Collection;
import java.util.stream.Collectors;

/**
 * Take N the same products and get discount
 */
@DomainPolicy
public class MoreSameProductsWithDiscountRebate implements RebatePolicy {

    private final ProductCode discountedProductCode;
    private final int numberOfRequiredProducts;
    private final Money discount;
    private final String rebateName;

    public MoreSameProductsWithDiscountRebate(ProductCode discountedProductCode, int numberOfRequiredProducts, Money discount) {
        this.discountedProductCode = discountedProductCode;
        this.numberOfRequiredProducts = numberOfRequiredProducts;
        this.discount = discount;
        this.rebateName = String.format("%d products with code %s with discount %.2f", numberOfRequiredProducts, discountedProductCode.getProductCodeName(), discount.getAmount().doubleValue());
    }

    @Override
    public Rebate calculateRebate(Collection<Product> products) {
        Collection<Product> sameProducts = products.stream().filter(product -> product.getProductCode().equals(discountedProductCode)).collect(Collectors.toList());
        int numberOfSameProducts = sameProducts.size();
        if (numberOfSameProducts < numberOfRequiredProducts) {
            return new Rebate(Money.ZERO, "NO APPLIED");
        }

        int quantityOfCalculatedPromotion = (numberOfSameProducts / numberOfRequiredProducts);

        Money calculatedDiscount = discount.multiplyBy(quantityOfCalculatedPromotion);

        return new Rebate(calculatedDiscount, rebateName);
    }
}
