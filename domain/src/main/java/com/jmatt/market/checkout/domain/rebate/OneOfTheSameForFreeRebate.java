package com.jmatt.market.checkout.domain.rebate;

import com.jmatt.market.checkout.domain.commons.DomainPolicy;
import com.jmatt.market.checkout.domain.commons.Money;
import com.jmatt.market.checkout.domain.product.Product;
import com.jmatt.market.checkout.domain.product.ProductCode;
import com.jmatt.market.checkout.domain.product.ProductNotFoundException;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Take N the same products and get one for free
 */
@DomainPolicy
public class OneOfTheSameForFreeRebate implements RebatePolicy {
    private final ProductCode discountedProductCode;
    private final int numberOfRequiredProducts;
    private final String rebateName;

    public OneOfTheSameForFreeRebate(ProductCode discountedProductCode, int numberOfRequiredProducts) {
        this.discountedProductCode = discountedProductCode;
        this.numberOfRequiredProducts = numberOfRequiredProducts;
        this.rebateName = String.format("%d products with code %s and one for free", numberOfRequiredProducts, discountedProductCode.getProductCodeName());
    }

    @Override
    public Rebate calculateRebate(Collection<Product> products) {
        Collection<Product> sameProducts = products.stream().filter(product -> product.getProductCode().equals(discountedProductCode)).collect(Collectors.toList());
        int numberOfSameProducts = sameProducts.size();
        if (numberOfSameProducts < numberOfRequiredProducts) {
            return new Rebate(Money.ZERO, "NO APPLIED");
        }

        Optional<Product> optionalProduct = sameProducts.stream().findAny();

        var product = optionalProduct.orElseThrow(ProductNotFoundException::new);

        int quantityOfCalculatedPromotion = (numberOfSameProducts / numberOfRequiredProducts);

        Money calculatedDiscount = product.getProductPrice().multiplyBy(quantityOfCalculatedPromotion);

        return new Rebate(calculatedDiscount, rebateName);
    }
}
