package com.jmatt.market.checkout.domain.rebate;

import com.jmatt.market.checkout.domain.commons.Money;

public class Rebate {
    private final Money discount;
    private final String rebateName;


    public Rebate(Money discount, String rebateName) {
        this.discount = discount;
        this.rebateName = rebateName;
    }

    public Money getDiscount() {
        return discount;
    }

    public String getRebateName() {
        return rebateName;
    }
}
