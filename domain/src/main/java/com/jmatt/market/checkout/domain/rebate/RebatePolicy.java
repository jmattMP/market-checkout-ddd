package com.jmatt.market.checkout.domain.rebate;

import com.jmatt.market.checkout.domain.product.Product;

import java.util.Collection;

public interface RebatePolicy {
    /**
     * calculates rebate based on products
     */
    Rebate calculateRebate(Collection<Product> products);
}
