package com.jmatt.market.checkout.domain.rebate;

import com.jmatt.market.checkout.domain.order.Order;

public interface RebateProvider {
    Rebate calculateRebate(Order order);
}
