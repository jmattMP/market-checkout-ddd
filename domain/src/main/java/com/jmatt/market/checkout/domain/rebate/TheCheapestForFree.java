package com.jmatt.market.checkout.domain.rebate;

import com.jmatt.market.checkout.domain.commons.DomainPolicy;
import com.jmatt.market.checkout.domain.commons.Money;
import com.jmatt.market.checkout.domain.product.Product;

import java.util.Collection;
import java.util.Comparator;
import java.util.stream.Collectors;

/**
 * Take at least N different products and get the cheapest for free
 */
@DomainPolicy
public class TheCheapestForFree implements RebatePolicy {
    private final int requiredNumberOfProducts;
    private final String rebateName;

    public TheCheapestForFree(int requiredNumberOfProducts) {
        this.requiredNumberOfProducts = requiredNumberOfProducts;
        this.rebateName = String.format("%d different products and the cheapest for free", requiredNumberOfProducts);
    }

    @Override
    public Rebate calculateRebate(Collection<Product> products) {
        Collection<Product> distinctProducts = products.stream().distinct().collect(Collectors.toList());
        if (distinctProducts.size() < requiredNumberOfProducts) {
            return new Rebate(Money.ZERO, "NO APPLIED");
        }

        Product cheapestProduct = distinctProducts.stream().min(Comparator.comparing(product -> product.getProductPrice().getAmount().longValue())).get();
        return new Rebate(cheapestProduct.getProductPrice(), rebateName);
    }
}
