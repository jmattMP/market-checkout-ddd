package com.jmatt.market.checkout.domain.rebate;

import com.jmatt.market.checkout.domain.commons.DomainPolicy;
import com.jmatt.market.checkout.domain.commons.Money;
import com.jmatt.market.checkout.domain.product.Product;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * Take N different products and get N% cheaper
 */
@DomainPolicy
public class TogetherCheaperRebate implements RebatePolicy {

    private final Collection<Product> discountedProducts;
    private final double percentageDiscount;
    private final String rebateName;

    public TogetherCheaperRebate(Collection<Product> discountedProducts, double percentageDiscount) {
        this.discountedProducts = discountedProducts;
        this.percentageDiscount = percentageDiscount / 100;
        this.rebateName = String.format("%d different products %.2f percent cheaper", discountedProducts.size(), percentageDiscount);
    }

    @Override
    public Rebate calculateRebate(Collection<Product> products) {
        Collection<Product> promotedProducts = products.stream().
                filter(discountedProducts::contains)
                .collect(Collectors.toList());

        Collection<Product> promotedDistinctProducts = promotedProducts.stream().distinct().collect(Collectors.toList());

        if (promotedDistinctProducts.size() < discountedProducts.size()) {
            return new Rebate(Money.ZERO, "NO APPLIED");
        }

        int quantityOfPromotion = (promotedProducts.size() / promotedDistinctProducts.size());

        BigDecimal costOfSinglePromotion = promotedDistinctProducts.stream()
                .map(product -> product.getProductPrice().getAmount())
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        Money totalCost = Money.from(costOfSinglePromotion.doubleValue() * quantityOfPromotion);

        Money calculatedDiscount = totalCost.multiplyBy(percentageDiscount);

        return new Rebate(calculatedDiscount, rebateName);
    }
}
