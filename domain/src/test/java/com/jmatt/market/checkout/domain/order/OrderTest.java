package com.jmatt.market.checkout.domain.order;

import com.jmatt.market.checkout.domain.commons.Money;
import com.jmatt.market.checkout.domain.product.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class OrderTest {
    private Order order;
    private Product discountedProductA;
    private Product discountedProductB;
    private Product discountedProductC;
    private Product discountedProductD;

    @BeforeEach
    void setUp() {
        order = Order.createOrder();

        var productNameA = ProductName.from("Product A");
        var productTypeA = ProductType.GROCERIES;
        var productTagsA = List.of(ProductTag.from("healthy food"));
        var productCodeA = ProductCode.ProductSkuGenerator.generateSkuBasedOn(productNameA, productTypeA, productTagsA);

        var productNameB = ProductName.from("Product B");
        var productTypeB = ProductType.GROCERIES;
        var productTagsB = List.of(ProductTag.from("meat"));
        var productCodeB = ProductCode.ProductSkuGenerator.generateSkuBasedOn(productNameB, productTypeB, productTagsB);

        var productNameC = ProductName.from("Product C");
        var productTypeC = ProductType.COSMETICS;
        var productTagsC = List.of(ProductTag.from("Deodorant"), ProductTag.from("Fresh"));
        var productCodeC = ProductCode.ProductSkuGenerator.generateSkuBasedOn(productNameC, productTypeC, productTagsC);

        var productNameD = ProductName.from("Product D");
        var productTypeD = ProductType.HEALTH;
        var productTagsD = List.of(ProductTag.from("Vitamins"), ProductTag.from("Minerals"));
        var productCodeD = ProductCode.ProductSkuGenerator.generateSkuBasedOn(productNameD, productTypeD, productTagsD);

        discountedProductA = new Product(
                productNameA, productCodeA, Money.from(40), LocalDate.of(2021, Month.MAY, 1), productTypeA, productTagsA);
        discountedProductB = new Product(
                ProductName.from("Product B"), productCodeB, Money.from(10), LocalDate.of(2020, Month.MAY, 1), productTypeB, productTagsB);
        discountedProductC = new Product(
                ProductName.from("Product C"), productCodeC, Money.from(30), LocalDate.of(2021, Month.MAY, 1), productTypeC, productTagsC);
        discountedProductD = new Product(
                ProductName.from("Product D"), productCodeD, Money.from(25), LocalDate.of(2021, Month.MAY, 1), productTypeD, productTagsD);
    }

    @Test
    void addProductToOrder() {
        order.addItem(discountedProductA);
        order.addItem(discountedProductB);
        order.addItem(discountedProductC);
        order.addItem(discountedProductD);


        assertThat(order.getOrderItems()).hasSize(4);
        assertThat(order.getOrderItems()).containsExactly(new OrderItem(discountedProductA), new OrderItem(discountedProductB), new OrderItem(discountedProductC), new OrderItem(discountedProductD));
    }

    @Test
    void removeProductFromOrder() {
        order.addItem(discountedProductA);
        order.addItem(discountedProductB);
        order.addItem(discountedProductC);
        order.addItem(discountedProductD);
        var testedOrder = order.removeItem(discountedProductA.getProductId());

        assertThat(testedOrder.getOrderItems()).hasSize(3);
        assertThat(testedOrder.getOrderItems()).containsExactly(new OrderItem(discountedProductB), new OrderItem(discountedProductC), new OrderItem(discountedProductD));
    }

    @Test
    void submitOrder() {
        order.addItem(discountedProductA);
        order.addItem(discountedProductB);
        order.addItem(discountedProductC);
        order.addItem(discountedProductD);
        order.removeItem(discountedProductA.getProductId());
        var testedOrder = order.submitOrder();

        assertThat(order.getOrderItems()).hasSize(3);
        assertThat(order.getOrderItems()).containsExactly(new OrderItem(discountedProductB), new OrderItem(discountedProductC), new OrderItem(discountedProductD));
        assertThat(order.getTotalPrice()).isEqualByComparingTo(testedOrder.getTotalPrice());
    }
}