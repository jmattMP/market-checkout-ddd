package com.jmatt.market.checkout.domain.product;

import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class ProductCodeTest {

    @Test
    void productNameCannotBeNull() {

        ProductName productNameC = null;
        var productTypeC = ProductType.COSMETICS;
        var productTagsC = List.of(ProductTag.from("Deodorant"), ProductTag.from("Fresh"));

        assertThatThrownBy(
                () -> ProductCode.ProductSkuGenerator.generateSkuBasedOn(productNameC, productTypeC, productTagsC)).
                isInstanceOf(IllegalArgumentException.class).
                hasMessageContaining("Product name and product Type could not be null");
    }

    @Test
    void productTypeCannotBeNull() {
        var productNameC = ProductName.from("Product C");
        ProductType productTypeC = null;
        var productTagsC = List.of(ProductTag.from("Deodorant"), ProductTag.from("Fresh"));

        assertThatThrownBy(
                () -> ProductCode.ProductSkuGenerator.generateSkuBasedOn(productNameC, productTypeC, productTagsC)).
                isInstanceOf(IllegalArgumentException.class).
                hasMessageContaining("Product name and product Type could not be null");
    }

    @Test
    void productTagsCanBeNull() {
        var productNameC = ProductName.from("Product C");
        var productTypeC = ProductType.ENTERTAINMENT;
        Collection<ProductTag> productTagsC = null;

        ProductCode productCode = ProductCode.ProductSkuGenerator.generateSkuBasedOn(productNameC, productTypeC, productTagsC);

        assertThat(productCode.getProductCodeName()).isEqualTo("PR-EN");
    }

    @Test
    void createProductCodeAndEnsureCodeIsUpperCase() {
        var productNameC = ProductName.from("Product C");
        var productTypeC = ProductType.COSMETICS;
        var productTagsC = List.of(ProductTag.from("Deodorant"), ProductTag.from("Fresh"));
        ProductCode productCode = ProductCode.ProductSkuGenerator.generateSkuBasedOn(productNameC, productTypeC, productTagsC);

        assertThat(productCode.getProductCodeName()).isEqualTo("PR-CO-DE-FR");
    }
}
