package com.jmatt.market.checkout.domain.product;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class ProductNameTest {


    @Test
    void createProductName() {
        ProductName productName = ProductName.from("coco coco soft drink");

        assertThat(productName.getProductName()).isEqualTo("Coco Coco Soft Drink");
    }

    @Test
    void productNameIsTitleCapitalized() {
        ProductName productName = ProductName.from("jmAtt coTTage cheeSe 150G");

        assertThat(productName.getProductName()).isEqualTo("Jmatt Cottage Cheese 150g");
    }

    @Test
    void productNameCannotBeBlank() {
        assertThatThrownBy(
                () -> ProductName.from("")).
                isInstanceOf(IllegalArgumentException.class).
                hasMessageContaining("Product name cannot be null, blank, empty or less then 5 chars.");
    }

    @Test
    void productNameCannotBeEmpty() {
        assertThatThrownBy(
                () -> ProductName.from(null)).
                isInstanceOf(IllegalArgumentException.class).
                hasMessageContaining("Product name cannot be null, blank, empty or less then 5 chars.");
    }

    @Test
    void productNameMustHaveAtLeastFiveChars() {
        assertThatThrownBy(
                () -> ProductName.from("1234")).
                isInstanceOf(IllegalArgumentException.class).
                hasMessageContaining("Product name cannot be null, blank, empty or less then 5 chars.");
    }
}