package com.jmatt.market.checkout.domain.product;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class ProductTagTest {

    @Test
    void createProductTag() {
        ProductTag productTag = ProductTag.from("garden chairs");

        assertThat(productTag.getProductTag()).isEqualTo("Garden Chairs");
    }

    @Test
    void productTagIsTitleCapitalized() {
        ProductTag productTag = ProductTag.from("soFT drInk");

        assertThat(productTag.getProductTag()).isEqualTo("Soft Drink");
    }

    @Test
    void productTagCannotBeBlank() {
        assertThatThrownBy(
                () -> ProductTag.from("")).
                isInstanceOf(IllegalArgumentException.class).
                hasMessageContaining("Product tag cannot be null, blank, empty or less then 4 chars.");
    }

    @Test
    void productTagCannotBeEmpty() {
        assertThatThrownBy(
                () -> ProductTag.from(null)).
                isInstanceOf(IllegalArgumentException.class).
                hasMessageContaining("Product tag cannot be null, blank, empty or less then 4 chars.");
    }

    @Test
    void productTagMustContainsAtLeastFourChars() {
        assertThatThrownBy(
                () -> ProductTag.from("123")).
                isInstanceOf(IllegalArgumentException.class).
                hasMessageContaining("Product tag cannot be null, blank, empty or less then 4 chars.");
    }


}