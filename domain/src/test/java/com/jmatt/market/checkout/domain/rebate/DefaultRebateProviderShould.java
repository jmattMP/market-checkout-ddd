package com.jmatt.market.checkout.domain.rebate;

import com.jmatt.market.checkout.domain.commons.Money;
import com.jmatt.market.checkout.domain.order.Order;
import com.jmatt.market.checkout.domain.product.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.Month;
import java.util.Collection;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class DefaultRebateProviderShould {

    private RebateProvider rebateProvider;
    private Collection<Product> fourCProductsWin;
    private Collection<Product> threeAProductsWin;
    private Collection<Product> pApBpCWin;
    private Collection<Product> threeDifferent;

    @BeforeEach
    void setUp() {
        ProductName productNameA = ProductName.from("Product A");
        ProductType productTypeA = ProductType.GROCERIES;
        Collection<ProductTag> productTagsA = List.of(ProductTag.from("healthy food"));
        ProductCode productCodeA = ProductCode.ProductSkuGenerator.generateSkuBasedOn(productNameA, productTypeA, productTagsA);

        ProductName productNameB = ProductName.from("Product B");
        ProductType productTypeB = ProductType.GROCERIES;
        Collection<ProductTag> productTagsB = List.of(ProductTag.from("meat"));
        ProductCode productCodeB = ProductCode.ProductSkuGenerator.generateSkuBasedOn(productNameB, productTypeB, productTagsB);

        ProductName productNameC = ProductName.from("Product C");
        ProductType productTypeC = ProductType.COSMETICS;
        Collection<ProductTag> productTagsC = List.of(ProductTag.from("Deodorant"), ProductTag.from("Fresh"));
        ProductCode productCodeC = ProductCode.ProductSkuGenerator.generateSkuBasedOn(productNameC, productTypeC, productTagsC);

        ProductName productNameD = ProductName.from("Product D");
        ProductType productTypeD = ProductType.HEALTH;
        Collection<ProductTag> productTagsD = List.of(ProductTag.from("Vitamins"), ProductTag.from("Minerals"));
        ProductCode productCodeD = ProductCode.ProductSkuGenerator.generateSkuBasedOn(productNameD, productTypeD, productTagsD);

        Product discountedProductA = new Product(
                productNameA, productCodeA, Money.from(40), LocalDate.of(2021, Month.MAY, 1), productTypeA, productTagsA);
        Product discountedProductB = new Product(
                ProductName.from("Product B"), productCodeB, Money.from(10), LocalDate.of(2020, Month.MAY, 1), productTypeB, productTagsB);
        Product discountedProductC = new Product(
                ProductName.from("Product C"), productCodeC, Money.from(30), LocalDate.of(2021, Month.MAY, 1), productTypeC, productTagsC);
        Product discountedProductD = new Product(
                ProductName.from("Product D"), productCodeD, Money.from(25), LocalDate.of(2021, Month.MAY, 1), productTypeD, productTagsD);

        fourCProductsWin = List.of(discountedProductC, discountedProductC, discountedProductC, discountedProductC, discountedProductA, discountedProductA, discountedProductA, discountedProductB, discountedProductD);
        threeAProductsWin = List.of(discountedProductA, discountedProductA, discountedProductA, discountedProductB, discountedProductC, discountedProductD);
        pApBpCWin = List.of(discountedProductA, discountedProductB, discountedProductC, discountedProductD);
        threeDifferent = List.of(discountedProductA, discountedProductB, discountedProductC);

        Collection<RebatePolicy> rebatePolicies = List.of(
                new MoreSameProductsWithDiscountRebate(discountedProductC.getProductCode(), 4, Money.from(60)),
                new MoreSameProductsWithDiscountRebate(discountedProductA.getProductCode(), 3, Money.from(50)),
                new TheCheapestForFree(3),
                new TogetherCheaperRebate(List.of(discountedProductA, discountedProductB, discountedProductC, discountedProductD), 10.0));
        rebateProvider = new DefaultRebateProvider(rebatePolicies);

    }

    @Test
    void calculateFourTheSameProductsCheaperRebate() {
        Order order = Order.createOrder();
        fourCProductsWin.forEach(order::addItem);

        Rebate calculatedRebate = rebateProvider.calculateRebate(order);

        assertThat(calculatedRebate.getDiscount()).isEqualByComparingTo(Money.from(60));
    }

    @Test
    void calculateThreeTheSameProductsCheaperRebate() {
        Order order = Order.createOrder();
        threeAProductsWin.forEach(order::addItem);

        Rebate calculatedRebate = rebateProvider.calculateRebate(order);

        assertThat(calculatedRebate.getDiscount()).isEqualByComparingTo(Money.from(50));
    }

    @Test
    void calculateTenPercentageForFourDifferentCheaperRebate() {
        Order order = Order.createOrder();
        pApBpCWin.forEach(order::addItem);

        Rebate calculatedRebate = rebateProvider.calculateRebate(order);

        assertThat(calculatedRebate.getDiscount()).isEqualByComparingTo(Money.from(10.5));
    }

    @Test
    void calculateTheCheapestForFreeRebate() {
        Order order = Order.createOrder();
        threeDifferent.forEach(order::addItem);

        Rebate calculatedRebate = rebateProvider.calculateRebate(order);

        assertThat(calculatedRebate.getDiscount()).isEqualByComparingTo(Money.from(10));
    }
}