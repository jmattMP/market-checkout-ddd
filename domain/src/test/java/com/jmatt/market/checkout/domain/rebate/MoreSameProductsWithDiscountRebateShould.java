package com.jmatt.market.checkout.domain.rebate;

import com.jmatt.market.checkout.domain.commons.Money;
import com.jmatt.market.checkout.domain.product.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.Month;
import java.util.Collection;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class MoreSameProductsWithDiscountRebateShould {

    private MoreSameProductsWithDiscountRebate threeSameProducts50LessRebate;
    private Product discountedProductA;
    private Product discountedProductB;
    private Collection<Product> products;

    @BeforeEach
    void setUp() {
        var productNameA = ProductName.from("Product A");
        var productTypeA = ProductType.GROCERIES;
        var productTagsA = List.of(ProductTag.from("healthy food"));
        var productCodeA = ProductCode.ProductSkuGenerator.generateSkuBasedOn(productNameA, productTypeA, productTagsA);

        var productNameB = ProductName.from("Product B");
        var productTypeB = ProductType.GROCERIES;
        var productTagsB = List.of(ProductTag.from("meat"));
        var productCodeB = ProductCode.ProductSkuGenerator.generateSkuBasedOn(productNameB, productTypeB, productTagsB);

        var productNameC = ProductName.from("Product C");
        var productTypeC = ProductType.COSMETICS;
        var productTagsC = List.of(ProductTag.from("Deodorant"), ProductTag.from("Fresh"));
        var productCodeC = ProductCode.ProductSkuGenerator.generateSkuBasedOn(productNameC, productTypeC, productTagsC);

        var productNameD = ProductName.from("Product D");
        var productTypeD = ProductType.HEALTH;
        var productTagsD = List.of(ProductTag.from("Vitamins"), ProductTag.from("Minerals"));
        var productCodeD = ProductCode.ProductSkuGenerator.generateSkuBasedOn(productNameD, productTypeD, productTagsD);

        discountedProductA = new Product(
                productNameA, productCodeA, Money.from(40), LocalDate.of(2021, Month.MAY, 1), productTypeA, productTagsA);
        discountedProductB = new Product(
                ProductName.from("Product B"), productCodeB, Money.from(10), LocalDate.of(2020, Month.MAY, 1), productTypeB, productTagsB);
        Product discountedProductC = new Product(
                ProductName.from("Product C"), productCodeC, Money.from(30), LocalDate.of(2021, Month.MAY, 1), productTypeC, productTagsC);
        Product discountedProductD = new Product(
                ProductName.from("Product D"), productCodeD, Money.from(25), LocalDate.of(2021, Month.MAY, 1), productTypeD, productTagsD);
        products = List.of(discountedProductA, discountedProductB, discountedProductC, discountedProductD);

        threeSameProducts50LessRebate = new MoreSameProductsWithDiscountRebate(discountedProductA.getProductCode(), 3, Money.from(50));
    }

    @Test
    void noCalculateRebateWhenLessThenRequired() {
        Rebate calculatedRebate = threeSameProducts50LessRebate.calculateRebate(List.of(discountedProductA, discountedProductA));

        assertThat(calculatedRebate.getDiscount()).isEqualByComparingTo(Money.ZERO);
    }

    @Test
    void noCalculateRebateWhenDifferentProducts() {
        Rebate calculatedRebate = threeSameProducts50LessRebate.calculateRebate(products);

        assertThat(calculatedRebate.getDiscount()).isEqualByComparingTo(Money.ZERO);
    }

    @Test
    void calculateRebateWhenExactlyNumberOfRequired() {
        Rebate calculatedRebate = threeSameProducts50LessRebate.calculateRebate(List.of(discountedProductA, discountedProductA, discountedProductA));

        assertThat(calculatedRebate.getDiscount()).isEqualByComparingTo(Money.from(50));
    }

    @Test
    void calculateRebateWhenMoreThenRequired() {
        Rebate calculatedRebate = threeSameProducts50LessRebate.calculateRebate(List.of(discountedProductA, discountedProductA, discountedProductA, discountedProductA, discountedProductB));

        assertThat(calculatedRebate.getDiscount()).isEqualByComparingTo(Money.from(50));
    }

    @Test
    void calculateDoubleRebateWhenDoubleNumberOfRequired() {
        Rebate calculatedRebate = threeSameProducts50LessRebate.calculateRebate(List.of(discountedProductA, discountedProductA, discountedProductA, discountedProductA, discountedProductA, discountedProductA, discountedProductB));

        assertThat(calculatedRebate.getDiscount()).isEqualByComparingTo(Money.from(100));
    }
}