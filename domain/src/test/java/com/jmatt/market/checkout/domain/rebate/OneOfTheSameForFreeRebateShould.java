package com.jmatt.market.checkout.domain.rebate;

import com.jmatt.market.checkout.domain.commons.Money;
import com.jmatt.market.checkout.domain.product.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class OneOfTheSameForFreeRebateShould {

    private OneOfTheSameForFreeRebate threeForTwoRebate;
    private Product discountedProductA;
    private Product discountedProductB;
    private Product discountedProductC;
    private Product discountedProductD;

    @BeforeEach
    void setUp() {
        var productNameA = ProductName.from("Product A");
        var productTypeA = ProductType.GROCERIES;
        var productTagsA = List.of(ProductTag.from("healthy food"));
        var productCodeA = ProductCode.ProductSkuGenerator.generateSkuBasedOn(productNameA, productTypeA, productTagsA);

        var productNameB = ProductName.from("Product B");
        var productTypeB = ProductType.GROCERIES;
        var productTagsB = List.of(ProductTag.from("meat"));
        var productCodeB = ProductCode.ProductSkuGenerator.generateSkuBasedOn(productNameB, productTypeB, productTagsB);

        var productNameC = ProductName.from("Product C");
        var productTypeC = ProductType.COSMETICS;
        var productTagsC = List.of(ProductTag.from("Deodorant"), ProductTag.from("Fresh"));
        var productCodeC = ProductCode.ProductSkuGenerator.generateSkuBasedOn(productNameC, productTypeC, productTagsC);

        var productNameD = ProductName.from("Product D");
        var productTypeD = ProductType.HEALTH;
        var productTagsD = List.of(ProductTag.from("Vitamins"), ProductTag.from("Minerals"));
        var productCodeD = ProductCode.ProductSkuGenerator.generateSkuBasedOn(productNameD, productTypeD, productTagsD);

        discountedProductA = new Product(
                productNameA, productCodeA, Money.from(40), LocalDate.of(2021, Month.MAY, 1), productTypeA, productTagsA);
        discountedProductB = new Product(
                ProductName.from("Product B"), productCodeB, Money.from(10), LocalDate.of(2020, Month.MAY, 1), productTypeB, productTagsB);
        discountedProductC = new Product(
                ProductName.from("Product C"), productCodeC, Money.from(30), LocalDate.of(2021, Month.MAY, 1), productTypeC, productTagsC);
        discountedProductD = new Product(
                ProductName.from("Product D"), productCodeD, Money.from(25), LocalDate.of(2021, Month.MAY, 1), productTypeD, productTagsD);

        threeForTwoRebate = new OneOfTheSameForFreeRebate(discountedProductA.getProductCode(), 3);
    }

    @Test
    void noCalculateRebateWhenLessThenRequired() {
        Rebate calculatedRebate = threeForTwoRebate.calculateRebate(List.of(discountedProductA, discountedProductB, discountedProductC));

        assertThat(calculatedRebate.getDiscount()).isEqualByComparingTo(Money.ZERO);
    }

    @Test
    void noCalculateRebateWhenNoDiscountedProducts() {
        Rebate calculatedRebate = threeForTwoRebate.calculateRebate(List.of(discountedProductA, discountedProductA, discountedProductB, discountedProductB));

        assertThat(calculatedRebate.getDiscount()).isEqualByComparingTo(Money.ZERO);
    }

    @Test
    void calculateRebateWhenExactlyNumberOfRequired() {
        Rebate calculatedRebate = threeForTwoRebate.calculateRebate(List.of(discountedProductA, discountedProductA, discountedProductA));

        assertThat(calculatedRebate.getDiscount()).isEqualByComparingTo(Money.from(40));
    }

    @Test
    void calculateRebateWhenMoreThenRequired() {
        Rebate calculatedRebate = threeForTwoRebate.calculateRebate(List.of(discountedProductA, discountedProductA, discountedProductA, discountedProductD, discountedProductA));

        assertThat(calculatedRebate.getDiscount()).isEqualByComparingTo(Money.from(40));
    }

    @Test
    void calculateDoubleRebateWhenDoubleNumberOfRequired() {
        Rebate calculatedRebate = threeForTwoRebate.calculateRebate(List.of(
                discountedProductA, discountedProductA, discountedProductA, discountedProductD,
                discountedProductA, discountedProductB, discountedProductA, discountedProductA));

        assertThat(calculatedRebate.getDiscount()).isEqualByComparingTo(Money.from(80));
    }
}