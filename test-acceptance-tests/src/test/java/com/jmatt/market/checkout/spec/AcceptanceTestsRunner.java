package com.jmatt.market.checkout.spec;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(monochrome = true,
        features = "src/test/resources/features",
        dryRun = false,
        glue = "com.jmatt.market.checkout",
        plugin = {"pretty", "html:target/cucumber"})

public class AcceptanceTestsRunner {
    //Run this
}
