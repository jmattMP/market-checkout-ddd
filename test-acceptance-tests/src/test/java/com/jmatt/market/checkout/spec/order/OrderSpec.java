package com.jmatt.market.checkout.spec.order;

import com.jmatt.market.checkout.application.dto.order.OrderSummary;
import com.jmatt.market.checkout.application.port.in.OrderProcessingService;
import com.jmatt.market.checkout.application.port.out.OrderRepository;
import com.jmatt.market.checkout.application.port.out.ProductRepository;
import com.jmatt.market.checkout.application.service.OrderProcessingServiceImpl;
import com.jmatt.market.checkout.domain.commons.Money;
import com.jmatt.market.checkout.domain.commons.Validator;
import com.jmatt.market.checkout.domain.order.Order;
import com.jmatt.market.checkout.domain.order.OrderItem;
import com.jmatt.market.checkout.domain.order.OrderNotFoundException;
import com.jmatt.market.checkout.domain.product.*;
import com.jmatt.market.checkout.domain.rebate.*;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.time.LocalDate;
import java.time.Month;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.*;

//TODO add parametrized tests and cover all promotion
public class OrderSpec {
    private OrderProcessingService orderProcessingService;
    private OrderItem processedOrderItem;
    private Product processedProduct;
    private String processedOrderId;
    private Order processedOrder;
    private OrderRepository orderRepository;

    @Before
    public void setUp() {
        orderRepository = mock(OrderRepository.class);
        ProductRepository productRepository = mock(ProductRepository.class);
        Validator<Product> productValidator = mock(Validator.class);
        when(productValidator.isValid(any())).thenReturn(true);


        ProductName productNameA = ProductName.from("Product A");
        ProductType productTypeA = ProductType.GROCERIES;
        Collection<ProductTag> productTagsA = List.of(ProductTag.from("healthy food"));
        ProductCode productCodeA = ProductCode.ProductSkuGenerator.generateSkuBasedOn(productNameA, productTypeA, productTagsA);

        ProductName productNameB = ProductName.from("Product B");
        ProductType productTypeB = ProductType.GROCERIES;
        Collection<ProductTag> productTagsB = List.of(ProductTag.from("meat"));
        ProductCode productCodeB = ProductCode.ProductSkuGenerator.generateSkuBasedOn(productNameB, productTypeB, productTagsB);

        ProductName productNameC = ProductName.from("Product C");
        ProductType productTypeC = ProductType.COSMETICS;
        Collection<ProductTag> productTagsC = List.of(ProductTag.from("Deodorant"), ProductTag.from("Fresh"));
        ProductCode productCodeC = ProductCode.ProductSkuGenerator.generateSkuBasedOn(productNameC, productTypeC, productTagsC);

        ProductName productNameD = ProductName.from("Product D");
        ProductType productTypeD = ProductType.HEALTH;
        Collection<ProductTag> productTagsD = List.of(ProductTag.from("Vitamins"), ProductTag.from("Minerals"));
        ProductCode productCodeD = ProductCode.ProductSkuGenerator.generateSkuBasedOn(productNameD, productTypeD, productTagsD);

        Product discountedProductA = new Product(
                productNameA, productCodeA, Money.from(40), LocalDate.of(2021, Month.MAY, 1), productTypeA, productTagsA);
        Product discountedProductB = new Product(
                ProductName.from("Product B"), productCodeB, Money.from(10), LocalDate.of(2020, Month.MAY, 1), productTypeB, productTagsB);
        Product discountedProductC = new Product(
                ProductName.from("Product C"), productCodeC, Money.from(30), LocalDate.of(2021, Month.MAY, 1), productTypeC, productTagsC);
        Product discountedProductD = new Product(
                ProductName.from("Product D"), productCodeD, Money.from(25), LocalDate.of(2021, Month.MAY, 1), productTypeD, productTagsD);

        Collection<RebatePolicy> rebatePolicies = List.of(
                new MoreSameProductsWithDiscountRebate(discountedProductA.getProductCode(), 3, Money.from(50)),
                new TheCheapestForFree(3),
                new TogetherCheaperRebate(List.of(discountedProductA, discountedProductB, discountedProductC, discountedProductD), 10.0));
        RebateProvider rebateProvider = new DefaultRebateProvider(rebatePolicies);
        orderProcessingService = new OrderProcessingServiceImpl(orderRepository, productRepository, productValidator, rebateProvider);
    }

    @Given("Order is created")
    public void orderIsCreated() {
        processedOrder = orderProcessingService.createOrder();
        processedOrderId = processedOrder.getOrderId();
    }

    @When("I scan item {string}")
    public void scanItem(String arg0) {
        when(orderRepository.getOrder(eq(processedOrderId))).thenReturn(Optional.of(processedOrder));
        ProductName productNameA = ProductName.from("Product A");
        ProductType productTypeA = ProductType.GROCERIES;
        Collection<ProductTag> productTagsA = List.of(ProductTag.from("healthy food"));
        ProductCode productCodeA = ProductCode.ProductSkuGenerator.generateSkuBasedOn(productNameA, productTypeA, productTagsA);

        processedProduct = new Product(
                productNameA, productCodeA, Money.from(40), LocalDate.of(2021, Month.MAY, 1), productTypeA, productTagsA);

        processedOrderItem = orderProcessingService.addProduct(processedProduct, processedOrderId);
    }

    @Then("order contains one item")
    public void orderContainsOneItem() {
        Order order = orderProcessingService.getOrder(processedOrderId);
        assertThat(orderProcessingService.getOrder(order.getOrderId()).getOrderItems()).hasSize(1);
        assertThat(orderProcessingService.getOrder(order.getOrderId()).getOrderItems()).containsExactly(processedOrderItem);
    }

    @When("I remove item {string}")
    public void removeItem(String arg0) {
        when(orderRepository.getOrder(eq(processedOrderId))).thenReturn(Optional.of(processedOrder));
        orderProcessingService.removeProduct(processedProduct.getProductId(), processedOrderId);
    }

    @Then("order contains no items")
    public void orderContainsNoItems() {
        Order order = orderProcessingService.getOrder(processedOrderId);
        assertThat(orderProcessingService.getOrder(order.getOrderId()).getOrderItems()).isEmpty();
    }

    @Then("receive price of scanned item")
    public void receivePriceOfScannedItem() {
        Order order = orderProcessingService.getOrder(processedOrderId);
        assertThat(order.getOrderItems()).contains(processedOrderItem);
    }

    @Then("I receive total price {int} of order")
    public void receiveTotalPriceOfOrder(int arg0) {
        Order order = orderProcessingService.getOrder(processedOrderId);
        Money totalSum = Money.from(arg0);
        assertThat(order.getTotalPrice()).isEqualByComparingTo(totalSum);
    }

    @And("I confirm order")
    public void confirmOrder() {
        orderProcessingService.confirmOrder(processedOrderId);
    }

    @Then("receive order details")
    public void receiveOrderDetails() {
        OrderSummary order = orderProcessingService.getOrderSummary(processedOrderId);
        assertThat(order.getTotalPrice()).isNotNull();
        assertThat(order.getTotalPriceWithCalculatedRebate()).isNotNull();
        assertThat(order.getRebate()).isNotNull();
    }

    @And("receive calculated rebate {int}")
    public void receiveCalculatedRebate(int arg0) {
        Money totalPriceWithCalculatedRebate = Money.from(arg0);
        assertThat(orderProcessingService.getOrderSummary(processedOrderId).getTotalPriceWithCalculatedRebate()).isEqualByComparingTo(totalPriceWithCalculatedRebate);
    }

    @When("I cancel order")
    public void cancelOrder() {
        when(orderRepository.removeOrder(processedOrderId)).thenReturn(true);
        orderProcessingService.cancelOrder(processedOrderId);
    }

    @Then("order is canceled")
    public void orderIsCanceled() {
        when(orderRepository.getOrder(eq(processedOrderId))).thenReturn(Optional.empty());
        assertThatThrownBy(() -> orderProcessingService.getOrder(processedOrderId))
                .isInstanceOf(OrderNotFoundException.class)
                .hasMessageContaining(String.format("Order with id %s not exist", processedOrderId));
    }
}