package com.jmatt.market.checkout.spec.payment;

import com.jmatt.market.checkout.application.dto.PaymentSummary;
import com.jmatt.market.checkout.application.dto.order.OrderSummary;
import com.jmatt.market.checkout.application.dto.payment.PaymentDetails;
import com.jmatt.market.checkout.application.dto.payment.PaymentMethod;
import com.jmatt.market.checkout.application.dto.payment.PaymentStatus;
import com.jmatt.market.checkout.application.port.in.OrderProcessingService;
import com.jmatt.market.checkout.application.port.in.PayForOrderUseCase;
import com.jmatt.market.checkout.application.port.out.OrderRepository;
import com.jmatt.market.checkout.application.port.out.PaymentGateway;
import com.jmatt.market.checkout.application.port.out.ProductRepository;
import com.jmatt.market.checkout.application.service.OrderProcessingServiceImpl;
import com.jmatt.market.checkout.application.service.PayForOrder;
import com.jmatt.market.checkout.domain.commons.Money;
import com.jmatt.market.checkout.domain.commons.Validator;
import com.jmatt.market.checkout.domain.order.Order;
import com.jmatt.market.checkout.domain.order.OrderItem;
import com.jmatt.market.checkout.domain.product.*;
import com.jmatt.market.checkout.domain.rebate.*;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.time.LocalDate;
import java.time.Month;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

//TODO posprzataj test, wrzuc do before, wyrzuc co nie potrzebne, przepatrz jeszcze payment.
public class PaymentSpec {
    private OrderProcessingService orderProcessingService;
    private PayForOrderUseCase payForOrderUseCase;
    private String processedOrderId;
    private PaymentSummary processedPaymentSummary;
    private OrderRepository orderRepository;
    private PaymentGateway paymentGateway;

    @Before
    public void setUp() {
        orderRepository = mock(OrderRepository.class);
        paymentGateway = mock(PaymentGateway.class);
        ProductRepository productRepository = mock(ProductRepository.class);
        Validator<Product> productValidator = mock(Validator.class);
        when(productValidator.isValid(any())).thenReturn(true);


        ProductName productNameA = ProductName.from("Product A");
        ProductType productTypeA = ProductType.GROCERIES;
        Collection<ProductTag> productTagsA = List.of(ProductTag.from("healthy food"));
        ProductCode productCodeA = ProductCode.ProductSkuGenerator.generateSkuBasedOn(productNameA, productTypeA, productTagsA);

        ProductName productNameB = ProductName.from("Product B");
        ProductType productTypeB = ProductType.GROCERIES;
        Collection<ProductTag> productTagsB = List.of(ProductTag.from("meat"));
        ProductCode productCodeB = ProductCode.ProductSkuGenerator.generateSkuBasedOn(productNameB, productTypeB, productTagsB);

        ProductName productNameC = ProductName.from("Product C");
        ProductType productTypeC = ProductType.COSMETICS;
        Collection<ProductTag> productTagsC = List.of(ProductTag.from("Deodorant"), ProductTag.from("Fresh"));
        ProductCode productCodeC = ProductCode.ProductSkuGenerator.generateSkuBasedOn(productNameC, productTypeC, productTagsC);

        ProductName productNameD = ProductName.from("Product D");
        ProductType productTypeD = ProductType.HEALTH;
        Collection<ProductTag> productTagsD = List.of(ProductTag.from("Vitamins"), ProductTag.from("Minerals"));
        ProductCode productCodeD = ProductCode.ProductSkuGenerator.generateSkuBasedOn(productNameD, productTypeD, productTagsD);

        Product discountedProductA = new Product(
                productNameA, productCodeA, Money.from(40), LocalDate.of(2021, Month.MAY, 1), productTypeA, productTagsA);
        Product discountedProductB = new Product(
                ProductName.from("Product B"), productCodeB, Money.from(10), LocalDate.of(2020, Month.MAY, 1), productTypeB, productTagsB);
        Product discountedProductC = new Product(
                ProductName.from("Product C"), productCodeC, Money.from(30), LocalDate.of(2021, Month.MAY, 1), productTypeC, productTagsC);
        Product discountedProductD = new Product(
                ProductName.from("Product D"), productCodeD, Money.from(25), LocalDate.of(2021, Month.MAY, 1), productTypeD, productTagsD);

        Collection<RebatePolicy> rebatePolicies = List.of(
                new MoreSameProductsWithDiscountRebate(discountedProductA.getProductCode(), 3, Money.from(50)),
                new TheCheapestForFree(3),
                new TogetherCheaperRebate(List.of(discountedProductA, discountedProductB, discountedProductC, discountedProductD), 10.0));
        RebateProvider rebateProvider = new DefaultRebateProvider(rebatePolicies);
        orderProcessingService = new OrderProcessingServiceImpl(orderRepository, productRepository, productValidator, rebateProvider);
        payForOrderUseCase = new PayForOrder(paymentGateway, orderRepository);
    }

    @Given("order is submitted")
    public void orderIsSubmitted() {
        Order processedOrder = orderProcessingService.createOrder();
        processedOrderId = processedOrder.getOrderId();
        when(orderRepository.getOrder(eq(processedOrderId))).thenReturn(Optional.of(processedOrder));
        ProductName productName = ProductName.from("product");
        ProductCode productCode = ProductCode.ProductSkuGenerator.generateSkuBasedOn(productName, ProductType.CLOTHING, Collections.emptyList());
        Product processedProduct = new Product.ProductBuilder(productName, productCode, Money.from(100)).build();
        OrderItem processedOrderItem = orderProcessingService.addProduct(processedProduct, processedOrderId);
        OrderSummary orderSummary = orderProcessingService.confirmOrder(processedOrderId);
    }

    @When("I pay for order with card")
    public void payForOrderWithCard() {
        when(paymentGateway.getPaymentGateway(any(PaymentDetails.class))).thenReturn(new PaymentSummary(PaymentStatus.PAID, processedOrderId, PaymentMethod.CARD));
        PaymentDetails paymentDetails = new PaymentDetails(processedOrderId, PaymentMethod.CARD);
        processedPaymentSummary = payForOrderUseCase.pay(paymentDetails);
    }

    @Then("I get confirmation of successful payment with card")
    public void getConfirmationOfSuccessfulPaymentWithCard() {
        assertThat(processedPaymentSummary.getOrderId()).isEqualTo(processedOrderId);
        assertThat(processedPaymentSummary.getPaymentStatus()).isEqualByComparingTo(PaymentStatus.PAID);
        assertThat(processedPaymentSummary.getPaymentMethod()).isEqualByComparingTo(PaymentMethod.CARD);
    }

    @Then("I get confirmation of successful payment with cash")
    public void getConfirmationOfSuccessfulPaymentWithCash() {
        assertThat(processedPaymentSummary.getPaymentStatus()).isEqualByComparingTo(PaymentStatus.PAID);
        assertThat(processedPaymentSummary.getPaymentMethod()).isEqualByComparingTo(PaymentMethod.CASH);
    }

    @When("I pay for order with cash")
    public void payForOrderWithCash() {
        when(paymentGateway.getPaymentGateway(any(PaymentDetails.class))).thenReturn(new PaymentSummary(PaymentStatus.PAID, processedOrderId, PaymentMethod.CASH));
        PaymentDetails paymentDetails = new PaymentDetails(processedOrderId, PaymentMethod.CASH);
        processedPaymentSummary = payForOrderUseCase.pay(paymentDetails);
    }

    @When("I pay for order with card with insufficient amount")
    public void payForOrderWithCardWithInsufficientAmount() {
        when(paymentGateway.getPaymentGateway(any(PaymentDetails.class))).thenReturn(new PaymentSummary(PaymentStatus.FAILED, processedOrderId, PaymentMethod.CARD));
        PaymentDetails paymentDetails = new PaymentDetails(processedOrderId, PaymentMethod.CARD);
        processedPaymentSummary = payForOrderUseCase.pay(paymentDetails);
    }

    @Then("I get refused payment")
    public void getRefusedPayment() {
        assertThat(processedPaymentSummary.getPaymentStatus()).isEqualByComparingTo(PaymentStatus.FAILED);
    }
}
