package com.jmatt.market.checkout.spec.receipt;

import com.jmatt.market.checkout.application.dto.PaymentSummary;
import com.jmatt.market.checkout.application.dto.order.OrderSummary;
import com.jmatt.market.checkout.application.dto.payment.PaymentDetails;
import com.jmatt.market.checkout.application.dto.payment.PaymentMethod;
import com.jmatt.market.checkout.application.dto.payment.PaymentStatus;
import com.jmatt.market.checkout.application.dto.receipt.ReceiptDetails;
import com.jmatt.market.checkout.application.port.in.OrderProcessingService;
import com.jmatt.market.checkout.application.port.in.PayForOrderUseCase;
import com.jmatt.market.checkout.application.port.in.PrintReceiptUseCase;
import com.jmatt.market.checkout.application.port.out.OrderRepository;
import com.jmatt.market.checkout.application.port.out.PaymentGateway;
import com.jmatt.market.checkout.application.port.out.ProductRepository;
import com.jmatt.market.checkout.application.service.OrderProcessingServiceImpl;
import com.jmatt.market.checkout.application.service.PayForOrder;
import com.jmatt.market.checkout.application.service.PrintReceiptService;
import com.jmatt.market.checkout.domain.commons.Money;
import com.jmatt.market.checkout.domain.commons.Validator;
import com.jmatt.market.checkout.domain.order.Order;
import com.jmatt.market.checkout.domain.order.OrderItem;
import com.jmatt.market.checkout.domain.product.*;
import com.jmatt.market.checkout.domain.rebate.*;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.time.LocalDate;
import java.time.Month;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ReceiptSpec {
    private OrderProcessingService orderProcessingService;
    private PayForOrderUseCase payForOrderUseCase;
    private PrintReceiptUseCase printReceiptUseCase;
    private String processedOrderId;
    private PaymentSummary processedPaymentSummary;
    private ReceiptDetails receiptDetails;
    private OrderItem processedOrderItem;
    private OrderRepository orderRepository;
    private PaymentGateway paymentGateway;
    private Order processedOrder;
    private OrderSummary orderSummary;
    private PaymentDetails paymentDetails;
    private Product discountedProductA;
    private Product discountedSecondProductA;
    private Product discountedThirdProductA;

    @Before
    public void setUp() {
        orderRepository = mock(OrderRepository.class);
        paymentGateway = mock(PaymentGateway.class);
        ProductRepository productRepository = mock(ProductRepository.class);
        Validator<Product> productValidator = mock(Validator.class);
        when(productValidator.isValid(any())).thenReturn(true);


        ProductName productNameA = ProductName.from("Product A");
        ProductType productTypeA = ProductType.GROCERIES;
        Collection<ProductTag> productTagsA = List.of(ProductTag.from("healthy food"));
        ProductCode productCodeA = ProductCode.ProductSkuGenerator.generateSkuBasedOn(productNameA, productTypeA, productTagsA);

        ProductName productNameB = ProductName.from("Product B");
        ProductType productTypeB = ProductType.GROCERIES;
        Collection<ProductTag> productTagsB = List.of(ProductTag.from("meat"));
        ProductCode productCodeB = ProductCode.ProductSkuGenerator.generateSkuBasedOn(productNameB, productTypeB, productTagsB);

        ProductName productNameC = ProductName.from("Product C");
        ProductType productTypeC = ProductType.COSMETICS;
        Collection<ProductTag> productTagsC = List.of(ProductTag.from("Deodorant"), ProductTag.from("Fresh"));
        ProductCode productCodeC = ProductCode.ProductSkuGenerator.generateSkuBasedOn(productNameC, productTypeC, productTagsC);

        ProductName productNameD = ProductName.from("Product D");
        ProductType productTypeD = ProductType.HEALTH;
        Collection<ProductTag> productTagsD = List.of(ProductTag.from("Vitamins"), ProductTag.from("Minerals"));
        ProductCode productCodeD = ProductCode.ProductSkuGenerator.generateSkuBasedOn(productNameD, productTypeD, productTagsD);

        discountedProductA = new Product(
                productNameA, productCodeA, Money.from(40), LocalDate.of(2021, Month.MAY, 1), productTypeA, productTagsA);
        discountedSecondProductA = new Product(
                productNameA, productCodeA, Money.from(40), LocalDate.of(2021, Month.MAY, 1), productTypeA, productTagsA);
        discountedThirdProductA = new Product(
                productNameA, productCodeA, Money.from(40), LocalDate.of(2021, Month.MAY, 1), productTypeA, productTagsA);
        Product discountedProductB = new Product(
                ProductName.from("Product B"), productCodeB, Money.from(10), LocalDate.of(2020, Month.MAY, 1), productTypeB, productTagsB);
        Product discountedProductC = new Product(
                ProductName.from("Product C"), productCodeC, Money.from(30), LocalDate.of(2021, Month.MAY, 1), productTypeC, productTagsC);
        Product discountedProductD = new Product(
                ProductName.from("Product D"), productCodeD, Money.from(25), LocalDate.of(2021, Month.MAY, 1), productTypeD, productTagsD);

        Collection<RebatePolicy> rebatePolicies = List.of(
                new MoreSameProductsWithDiscountRebate(discountedProductA.getProductCode(), 3, Money.from(50)),
                new TheCheapestForFree(3),
                new TogetherCheaperRebate(List.of(discountedProductA, discountedProductB, discountedProductC, discountedProductD), 10.0));
        RebateProvider rebateProvider = new DefaultRebateProvider(rebatePolicies);
        orderProcessingService = new OrderProcessingServiceImpl(orderRepository, productRepository, productValidator, rebateProvider);
        payForOrderUseCase = new PayForOrder(paymentGateway, orderRepository);

        paymentDetails = new PaymentDetails(processedOrderId, PaymentMethod.CARD);

        processedOrder = orderProcessingService.createOrder();
        processedOrderId = processedOrder.getOrderId();

        printReceiptUseCase = new PrintReceiptService(orderRepository, orderProcessingService);

    }

    @Given("Order is successfully paid")
    public void orderIsSuccessfullyPaid() {
        when(paymentGateway.getPaymentGateway(any(PaymentDetails.class))).thenReturn(new PaymentSummary(PaymentStatus.PAID, processedOrderId, PaymentMethod.CARD));
        when(orderRepository.getOrder(anyString())).thenReturn(Optional.of(processedOrder));

        orderProcessingService.addProduct(discountedProductA, processedOrderId);
        orderProcessingService.addProduct(discountedSecondProductA, processedOrderId);
        processedOrderItem = orderProcessingService.addProduct(discountedThirdProductA, processedOrderId);
        payForOrderUseCase.pay(paymentDetails);
        orderSummary = orderProcessingService.confirmOrder(processedOrderId);
    }

    @When("I ask for payment confirmation")
    public void askForPaymentConfirmation() {
        receiptDetails = printReceiptUseCase.getReceipt(processedOrderId);
    }

    @Then("receipt is printed with total price")
    public void receiptIsPrintedWithTotalPrice() {
        assertThat(receiptDetails.getOrderSummary().getTotalPrice()).isNotNull();
    }

    @Then("receipt is printed with all bought products")
    public void receiptIsPrintedWithAllBoughtProducts() {
        assertThat(receiptDetails.getOrderSummary().getOrderItems()).containsKeys(processedOrderItem.getProductId());
    }

    @Then("receipt is printed with all bought product prices")
    public void receiptIsPrintedWithAllBoughtProductPrices() {
        assertThat(receiptDetails.getOrderSummary().getOrderItems()).containsKeys(processedOrderItem.getProductId());
        assertThat(receiptDetails.getOrderSummary().getOrderItems().get(processedOrderItem.getProductId())).isNotNull();
    }

    @Then("receipt is printed with calculated rebate")
    public void receiptIsPrintedWithCalculatedRebate() {
        assertThat(receiptDetails.getOrderSummary().getRebate()).isNotNull();
        assertThat(receiptDetails.getOrderSummary().getRebate().getDiscount()).isEqualByComparingTo(Money.from(50));
        assertThat(receiptDetails.getOrderSummary().getRebate().getRebateName()).isEqualTo("3 products with code PR-GR-HE with discount 50,00");
    }
}
