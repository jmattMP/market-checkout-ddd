Feature: Manage order in market checkout
  As a customer who needs buy some items,
  I want to start ordering items
  So that I can buy items

  Acceptance criteria
  - I can scan(add) item (unless confirmed order)
  - I can remove item (unless confirmed order)
  - I can see price of scanned product
  - I can see total price of order in any time
  - I can confirm order in any time
  - I can see calculated rebate after confirm order
  - I can cancel order in any time (unless payment succeed)

  Scenario: adding item to order
    Given Order is created
    When I scan item "<item>"
    Then order contains one item

  Scenario: removing item from order
    Given Order is created
    When I scan item "<item>"
    When I remove item "<item>"
    Then order contains no items


  Scenario: seeing price of scanned product
    Given Order is created
    When I scan item "<item>"
    Then receive price of scanned item

  Scenario: seeing price of total price of order
    Given  Order is created
    When I scan item "<item>"
    And I scan item "<item>"
    And I scan item "<item>"
    Then I receive total price 120 of order

  Scenario: confirming order
    Given Order is created
    When I scan item "<item>"
    And I confirm order
    Then receive order details

  Scenario: seeing calculated rebate
    Given Order is created
    When I scan item "<item>"
    And I scan item "<item>"
    And I remove item "<item>"
    And I scan item "<item>"
    And I scan item "<item>"
    And I scan item "<item>"
    Then I confirm order
    And receive calculated rebate 120

  Scenario: canceling order
    Given Order is created
    When I scan item "<item>"
    And I scan item "<item>"
    When I cancel order
    Then order is canceled
