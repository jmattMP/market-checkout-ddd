Feature: Payment for confirmed order
  As a customer who ordered some items,
  I want to pay for them
  So that I can take them

  Acceptance criteria
  - I can choose payment with card
  - I can choose payment with cash

  Scenario: successful payment with card for order
    Given order is submitted
    When I pay for order with card
    Then I get confirmation of successful payment with card


  Scenario: successful payment with cash for order
    Given order is submitted
    When I pay for order with cash
    Then I get confirmation of successful payment with cash

  Scenario: unsuccessful payment with card for order if insufficient amount
    Given order is submitted
    When I pay for order with card with insufficient amount
    Then I get refused payment
