Feature: Receive receipt containing list of all products with corresponding prices
  As a customer who needs have confirmation for bought items,
  I want to get paper receipt
  So that I can have document of bought items

  Acceptance criteria
  - I receive paper receipt for bought items
  - Receipt contains list of all bought items
  - Receipt contains price of all bought items
  - Receipt contains total price
  - Receipt contains calculated rebate

  Scenario: printing receipt for paid order with total price
    Given Order is successfully paid
    When I ask for payment confirmation
    Then receipt is printed with total price

  Scenario: printing receipt for paid order with all bought products
    Given Order is successfully paid
    When I ask for payment confirmation
    Then receipt is printed with all bought products

  Scenario: printing receipts for paid order with all bought product prices
    Given Order is successfully paid
    When I ask for payment confirmation
    Then receipt is printed with all bought product prices

  Scenario: printing receipts for paid order with calculated rebate
    Given Order is successfully paid
    When I ask for payment confirmation
    Then receipt is printed with calculated rebate
